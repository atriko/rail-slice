using System;
using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts;
using Lean.Touch;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UniRx;
using UnityEngine;

public class Player : MonoSingleton<Player>
{
    [SerializeField] private Transform playerPlayerModel;
    [SerializeField] private Transform sawRoot;
    [SerializeField] private GameObject[] railSparks;
    [SerializeField] private BoxCollider blade;
    public Transform PlayerModel => playerPlayerModel;

    private Animator animator;
    private Rigidbody rb;
    private RagdollController ragdollController;
    private bool isFingerDown;

    private Vector3 targetPosition;
    private Vector3 rotationAngle;
    private Tween sizeTween;
    private float currentSize;
    private float tiltVelocity;
    private bool isFinishReached;
    private bool isOnLava;
    private const float SPARK_HEIGHT_OFFSET = 1.4f;

    private Vector2 screenDelta;
    private List<LeanFinger> fingers;
    private LeanFingerFilter use = new LeanFingerFilter(true);

    private IDisposable touchDisposable;
    private IDisposable horizontalMovement;
    private IDisposable rotationDisposable;
    private IDisposable forwardMovement;
    private IDisposable railCheckDisposable;
    private IDisposable lavaCheckDisposable;
    private IDisposable lavaZoneDisposable;

    private Tween resetRotationTween;

    public void Start()
    {
        animator = GetComponentInChildren<Animator>();
        ragdollController = GetComponentInChildren<RagdollController>();
        rb = GetComponent<Rigidbody>();
        EventManager.Instance.SubscribeEvent(GameEventType.GameStart, LevelStart);
    }

    private void LevelStart()
    {
        animator.CrossFade("RunWithBlade", 0f, 0);
        touchDisposable = Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(ListenTouch);
        horizontalMovement = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(HorizontalMovement);
        rotationDisposable = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(CharacterRotation);
        forwardMovement = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(ForwardMovement);
        railCheckDisposable = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(RailCheck);
        lavaCheckDisposable = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(LavaCheck);
    }

    private void ListenTouch(long obj)
    {
        fingers = use.UpdateAndGetFingers();
        screenDelta = LeanGesture.GetScaledDelta(fingers);
    }

    private void ForwardMovement(long obj)
    {
        transform.position += Vector3.forward * GameConfig.Instance.forwardSpeed / 10;
    }

    private void CharacterRotation(long obj)
    {
        var delta = screenDelta;
        delta.x = Mathf.Clamp(screenDelta.x, -GameConfig.Instance.rotationMaxAngle, GameConfig.Instance.rotationMaxAngle);
        rotationAngle.y = Mathf.SmoothDampAngle(playerPlayerModel.localEulerAngles.y, delta.x, ref tiltVelocity, GameConfig.Instance.rotationSmoothness);
        playerPlayerModel.localEulerAngles = rotationAngle;
    }

    private void HorizontalMovement(long obj)
    {
        targetPosition += Vector3.right * (screenDelta.x * GameConfig.Instance.sensitivity * 0.01f);
        playerPlayerModel.localPosition = Vector3.Lerp(playerPlayerModel.localPosition, targetPosition, Time.fixedDeltaTime * 7.5f);
    }

    private void RailCheck(long obj)
    {
        Collider[] results = new Collider[4];
        var size = Physics.OverlapBoxNonAlloc(blade.bounds.center, (blade.bounds.size + Vector3.up * 0.1f) / 2, results, Quaternion.Euler(Vector3.zero), LayerMask.GetMask("Rail"));
        if (size >= 2)
        {
            SetSparks(true);
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            SetSparkPositions(results);
        }
        else if (size == 1)
        {
            SetSparks(true);
            rb.constraints = RigidbodyConstraints.None;
        }
        else
        {
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            SetSparks(false);
        }

        if (rb.rotation.z > 0.2f || rb.rotation.z < -0.2f)
        {
            railCheckDisposable?.Dispose();
            if (isFinishReached)
            {
                DropOff();
            }
            else
            {
                FallOff();
            }
        }
    }

    private void LavaCheck(long l)
    {
        Ray ray = new Ray(PlayerModel.position + Vector3.up * 2f, Vector3.down);
        if (Physics.Raycast(ray, out var hit, 5f, LayerMask.GetMask("Lava")))
        {
            StartLosingBlade();
            isOnLava = true;
        }
        else
        {
            StopLosingBlade();
            isOnLava = false;
        }
    }

    private void DropOff()
    {
        if (!GameManager.Instance.IsGameEnded)
        {
            DisableControls();
            sawRoot.gameObject.AddComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            rb.rotation = Quaternion.Euler(Vector3.zero);
        }
    }

    public void GrowBlade()
    {
        sizeTween?.Kill(true);
        sizeTween = sawRoot.DOScaleX(sawRoot.localScale.x + GameConfig.Instance.bladeGrowAmount, 0.1f);
    }

    public void ShrinkBlade()
    {
        var newScale = sawRoot.localScale;
        newScale.x = sawRoot.localScale.x - GameConfig.Instance.bladeLoseAmount * 2;
        sawRoot.localScale = newScale;
        if (sawRoot.localScale.x <= 0.4f)
        {
            FallOff();
        }
    }

    public void SetNewBlade(BoxCollider newBlade)
    {
        blade = newBlade;
    }

    private void SetSparks(bool b)
    {
        railSparks.ForEach(o => o.SetActive(b));
    }

    private void SetSparkPositions(Collider[] railColliders)
    {
        for (int i = 0; i < railSparks.Length; i++)
        {
            var center = railColliders[i].transform.position;
            center.y = transform.position.y + SPARK_HEIGHT_OFFSET;
            center.z = transform.position.z;
            railSparks[i].transform.position = center;
        }
    }

    private void DisableControls()
    {
        SetSparks(false);
        touchDisposable?.Dispose();
        horizontalMovement?.Dispose();
        rotationDisposable?.Dispose();
        forwardMovement?.Dispose();
        railCheckDisposable?.Dispose();
        lavaZoneDisposable?.Dispose();
        lavaCheckDisposable?.Dispose();
    }

    public void FallOff()
    {
        if (!GameManager.Instance.IsGameEnded)
        {
            DisableControls();
            CameraController.Instance.DetachCamera();
            Destroy(rb);
            sawRoot.gameObject.AddComponent<Rigidbody>();
            ragdollController.EnableRagdoll();
            GameManager.Instance.LoseGame();
        }
    }

    public void FinishReached()
    {
        isFinishReached = true;
    }

    public void WinGame()
    {
        DisableControls();
        animator.CrossFade("Idle", 0, 0);
        rb.isKinematic = true;
        GameManager.Instance.WinGame();
        DropOff();
    }

    public void StartLosingBlade()
    {
        if (!isOnLava)
        {
            lavaZoneDisposable?.Dispose();
            lavaZoneDisposable = Observable.Interval(TimeSpan.FromSeconds(GameConfig.Instance.bladeLoseInterval)).TakeUntilDisable(this).Subscribe(LoseBlade);
        }
    }

    public void StopLosingBlade()
    {
        if (isOnLava)
        {
            lavaZoneDisposable?.Dispose();
        }
    }
    private void LoseBlade(long obj)
    {
        blade.GetComponent<Blade>().LosePiece();
    }
}