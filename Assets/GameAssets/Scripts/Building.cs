using GameAssets.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class Building : MonoBehaviour
{
    private void Awake()
    {
        transform.position += Vector3.up * Random.Range(GameConfig.Instance.minBuildingHeight,GameConfig.Instance.maxBuildingHeight);
    }
}
