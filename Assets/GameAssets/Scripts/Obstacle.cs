using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private bool isTriggered;
    private void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject.GetComponent<Player>();
        if (hit != null && !isTriggered)
        {
            isTriggered = true;
            FindObjectOfType<Blade>().GetCut(collision.GetContact(0).point);
        }
    }
}
