using System;
using DG.Tweening;
using EzySlice;
using GameAssets.Scripts;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

public class Blade : MonoBehaviour
{
    public void GetCut(Vector3 point)
    {
        var hull = gameObject.Slice(point, Player.Instance.PlayerModel.right);
        GameObject lostPiece;
        GameObject mainPiece;
        if (hull == null)
        {
            return;
        }

        if (point.x > Player.Instance.PlayerModel.position.x)
        {
            lostPiece = hull.CreateUpperHull(gameObject, GameConfig.Instance.bladeMaterial);
            mainPiece = hull.CreateLowerHull(gameObject, GameConfig.Instance.bladeMaterial);
        }
        else
        {
            lostPiece = hull.CreateLowerHull(gameObject, GameConfig.Instance.bladeMaterial);
            mainPiece = hull.CreateUpperHull(gameObject, GameConfig.Instance.bladeMaterial);
        }

        ResetPositions(mainPiece.transform);
        ResetPositions(lostPiece.transform);
        Launch(lostPiece);

        mainPiece.AddComponent<Blade>();
        var newCollider = mainPiece.AddComponent<BoxCollider>();
        Player.Instance.SetNewBlade(newCollider);

        Center(mainPiece);

        gameObject.SetActive(false);
    }

    private void Launch(GameObject lostPiece)
    {
        var rb = lostPiece.AddComponent<Rigidbody>();
        rb.velocity = Player.Instance.PlayerModel.forward * GameConfig.Instance.cutPieceForwardLaunch + Vector3.up * GameConfig.Instance.cutPieceUpwardsLaunch;
    }

    private void Center(GameObject mainPiece)
    {
        Observable.Timer(TimeSpan.FromSeconds(GameConfig.Instance.reCenterDelay)).TakeUntilDisable(mainPiece).Subscribe(delegate(long l)
        {
            var mainPieceBounds = mainPiece.GetComponent<MeshRenderer>().bounds;
            var centerDifference = mainPiece.transform.localPosition.x - transform.parent.InverseTransformPoint(mainPieceBounds.center).x;
            mainPiece.transform.DOLocalMoveX(centerDifference, 0.5f);
        });
    }

    private void ResetPositions(Transform piece)
    {
        piece.SetParent(transform.parent);
        var localPos = transform.localPosition;
        localPos.z = 0;
        piece.localPosition = localPos;
        piece.localEulerAngles = Vector3.zero;
        var localScale = transform.localScale;
        localScale.x = 1.4f;
        piece.localScale = localScale;
    }

    [Button]
    public void LosePiece()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        var leftPoint = meshRenderer.bounds.min;
        leftPoint.x += GameConfig.Instance.bladeLoseAmount;
        var rightPoint = meshRenderer.bounds.max;
        rightPoint.x -= GameConfig.Instance.bladeLoseAmount;

        var hull = gameObject.Slice(rightPoint, Vector3.right);
        var hull2 = gameObject.Slice(leftPoint, Vector3.right);
        
        var lostPiece = hull.CreateUpperHull(gameObject, GameConfig.Instance.bladeMaterial);
        var secondLostPiece = hull2.CreateLowerHull(gameObject, GameConfig.Instance.bladeMaterial);
        
        
        ResetPositions(lostPiece.transform);
        ResetPositions(secondLostPiece.transform);
        Launch(lostPiece);
        Launch(secondLostPiece);

        Player.Instance.ShrinkBlade();

    }
}