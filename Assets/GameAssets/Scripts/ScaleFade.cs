using DG.Tweening;
using UnityEngine;

public class ScaleFade : MonoBehaviour
{
    private void OnEnable()
    {
        transform.DOScale(Vector3.zero, 4f).OnComplete(() => gameObject.SetActive(false));
    }
}
