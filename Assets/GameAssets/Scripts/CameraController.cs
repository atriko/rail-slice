using Cinemachine;

public class CameraController : MonoSingleton<CameraController>
{
    private CinemachineVirtualCamera cam;

    private void Start()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
    }

    public void DetachCamera()
    {
        cam.Follow = null;
    }
}
