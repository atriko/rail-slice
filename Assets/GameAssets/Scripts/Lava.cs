using UnityEngine;

public class Lava : MonoBehaviour
{
    private bool isTriggered;

    private void OnTriggerEnter(Collider other)
    {
        var hit = other.GetComponentInParent<Player>();
        if (hit != null && !isTriggered)
        {
            isTriggered = true;
            hit.StartLosingBlade();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var hit = other.GetComponentInParent<Player>();
        if (hit != null && isTriggered)
        {
            isTriggered = false;
            hit.StopLosingBlade();
        }
    }
}
