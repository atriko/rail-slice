using DG.Tweening;
using EzySlice;
using GameAssets.Scripts;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    [SerializeField] private Material innerMaterial;
    private bool isTriggered;

   
    private void OnTriggerEnter(Collider other)
    {
        var hit = other.GetComponent<Blade>();
        if (hit != null && !isTriggered)
        {
            isTriggered = true;
            var hull = gameObject.Slice(transform.position, hit.transform.up);
            CreateUpperPiece(hull);
            CreateLowerPiece(hull);
            PoolManager.Instance.GetFromPool("Splat", other.ClosestPointOnBounds(transform.position - Vector3.forward));
            gameObject.SetActive(false);
        }
    }
    private void Start()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 0.1f, 1f).SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.OutQuad);
        transform.DOLocalRotate(new Vector3(transform.localEulerAngles.x, 180f, transform.localEulerAngles.z), 2f)
            .SetLoops(-1, LoopType.Incremental)
            .SetEase(Ease.Linear);
    }
    private void CreateUpperPiece(SlicedHull slicedHull)
    {
        var upperHull = slicedHull.CreateUpperHull(gameObject, innerMaterial);
        upperHull.transform.SetParent(transform.parent);
        upperHull.transform.localPosition = transform.localPosition;

        AddPhysics(upperHull);
    }


    private void CreateLowerPiece(SlicedHull slicedHull)
    {
        var lowerHull = slicedHull.CreateLowerHull(gameObject, innerMaterial);
        lowerHull.transform.SetParent(transform.parent);

        lowerHull.transform.localPosition = transform.localPosition;
        AddPhysics(lowerHull);
    }

    private void AddPhysics(GameObject hull)
    {
        // hull.AddComponent<MeshCollider>().convex = true;
        var rb = hull.AddComponent<Rigidbody>();
        rb.velocity = Player.Instance.PlayerModel.forward * Random.Range(GameConfig.Instance.fruitForwardMinCutPower, GameConfig.Instance.fruitForwardMaxCutPower) + Vector3.up * Random.Range(GameConfig.Instance.fruitUpwardsMinCutPower, GameConfig.Instance.fruitUpwardsMaxCutPower);
        rb.angularVelocity = Random.insideUnitSphere.normalized * Random.Range(10f, 15f);
        hull.AddComponent<ScaleFade>();
    }
}