using DG.Tweening;
using UnityEngine;

public class Popup : MonoBehaviour
{
    private void Start()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 1, 0.5f).OnComplete(() => gameObject.SetActive(false));
    }
}