using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI
{
    public class UpgradeButton : MonoBehaviour
    {
        [Title("Properties")]
        [SerializeField] private TextMeshProUGUI currencyText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private GameObject disableImage;

        public Action OnClicked;

        public void OnClick()
        {
            OnClicked?.Invoke();
        }
        public void UpdateCoinText(int coin, bool isMax) => currencyText.text = isMax ? "MAX" : $"${coin}";
        public void UpdateLevelText(int level) => levelText.text = $"Level {level}";
        public void SetActive(bool value) => disableImage.SetActive(!value);
    }
}