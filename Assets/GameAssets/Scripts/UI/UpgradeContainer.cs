using Framework.Game.Currency;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts.UI
{
    public class UpgradeContainer : MonoSingleton<UpgradeContainer>
    {
        public static UpgradeContainer Instance { get; private set; }

        [SerializeField] private UpgradeConfig config;
        [SerializeField] private Currency currency;

        [Title("Status Buttons")] [SerializeField]
        private UpgradeButton speedUpgrade;

        [SerializeField] private UpgradeButton powerUpgrade;
        [SerializeField] private UpgradeButton incomeUpgrade;


        private const string speedSaveKey = "_speed_upgrade_level";
        private const string damageSaveKey = "_power_upgrade_level";
        private const string IncomeSaveKey = "_income_upgrade_level";

        private void Awake() => Instance = this;

        private void Start()
        {
            currency.ObserveEveryValueChanged(currency1 => currency1.CurrentCurrency).TakeUntilDisable(this)
                .Subscribe(delegate(int i) { CheckStatusUpgrades(); });
            speedUpgrade.OnClicked += OnSpeedClicked;
            incomeUpgrade.OnClicked += OnIncomeClicked;
            powerUpgrade.OnClicked += OnPowerClicked;
        }
        #region Speed

        private bool SpeedIsAvailableToUpgrade()
        {
            return CoinRequiredForSpeed() <= currency.CurrentCurrency &&
                   GetSpeedLevel() < config.CoinRequiredSpeed.Length;
        }

        private int GetSpeedLevel() => PlayerPrefs.GetInt(speedSaveKey, 1);

        private void IncreaseSpeedLevel() =>
            PlayerPrefs.SetInt(speedSaveKey, GetSpeedLevel() + 1);

        private int CoinRequiredForSpeed() =>
            config.CoinRequiredSpeed[GetSpeedLevel() - 1];

        public float GetSpeedValue() =>
            config.SpeedUpgradeValues[GetSpeedLevel() - 1];

        private void OnSpeedClicked()
        {
            if (SpeedIsAvailableToUpgrade())
            {
                currency.SetCurrency(currency.CurrentCurrency - CoinRequiredForSpeed());
                IncreaseSpeedLevel();
            }

            CheckStatusUpgrades();
        }

        #endregion
        #region Income

        private bool IncomeIsAvailableToUpgrade()
        {
            return CoinRequiredForIncome() <= currency.CurrentCurrency &&
                   GetIncomeLevel() < config.CoinRequiredIncome.Length;
        }

        private int GetIncomeLevel() => PlayerPrefs.GetInt(IncomeSaveKey, 1);

        private void IncreaseIncomeLevel() =>
            PlayerPrefs.SetInt(IncomeSaveKey, GetIncomeLevel() + 1);

        private int CoinRequiredForIncome() => config.CoinRequiredIncome[GetIncomeLevel() - 1];

        public float GetIncomeValue() =>
            config.IncomeUpgradeValues[GetIncomeLevel() - 1];

        private void OnIncomeClicked()
        {
            if (IncomeIsAvailableToUpgrade())
            {
                currency.SetCurrency(currency.CurrentCurrency - CoinRequiredForIncome());
                IncreaseIncomeLevel();
            }

            CheckStatusUpgrades();
        }

        #endregion
        #region Power

        private bool PowerIsAvailableToUpgrade()
        {
            return CoinRequiredForPower() <= currency.CurrentCurrency &&
                   GetPowerLevel() < config.CoinRequiredPower.Length;
        }

        public int GetPowerLevel() => PlayerPrefs.GetInt(damageSaveKey, 1);

        private void IncreasePowerLevel() =>
            PlayerPrefs.SetInt(damageSaveKey, GetPowerLevel() + 1);

        private int CoinRequiredForPower() =>
            config.CoinRequiredPower[GetPowerLevel() - 1];

        public float GetPowerValue() =>
            config.PowerUpgradeValues[GetPowerLevel() - 1];

        private void OnPowerClicked()
        {
            if (PowerIsAvailableToUpgrade())
            {
                currency.SetCurrency(currency.CurrentCurrency - CoinRequiredForPower());

                IncreasePowerLevel();
            }

            CheckStatusUpgrades();
        }

        #endregion
        private void CheckStatusUpgrades()
        {
            speedUpgrade.UpdateLevelText(GetSpeedLevel());
            incomeUpgrade.UpdateLevelText(GetIncomeLevel());
            powerUpgrade.UpdateLevelText(GetPowerLevel());

            speedUpgrade.UpdateCoinText(CoinRequiredForSpeed(),
                GetSpeedLevel() >= config.CoinRequiredSpeed.Length);
            incomeUpgrade.UpdateCoinText(CoinRequiredForIncome(),
                GetIncomeLevel() >= config.CoinRequiredIncome.Length);
            powerUpgrade.UpdateCoinText(CoinRequiredForPower(),
                GetPowerLevel() >= config.CoinRequiredPower.Length);

            speedUpgrade.SetActive(SpeedIsAvailableToUpgrade());
            incomeUpgrade.SetActive(IncomeIsAvailableToUpgrade());
            powerUpgrade.SetActive(PowerIsAvailableToUpgrade());
        }
    }
}