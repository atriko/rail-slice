using DG.Tweening;
using UnityEngine;

public class Collectible : MonoBehaviour
{
   private bool isTriggered;

   private void OnTriggerEnter(Collider other)
   {
      var hit = other.GetComponentInParent<Player>();
      if (hit != null && !isTriggered)
      {
         isTriggered = true;
         gameObject.SetActive(false);
         UIManager.Instance.PopupMoney(transform.position,1);
         Player.Instance.GrowBlade();
      }
   }
   private void Start()
   {
      transform.DOLocalMoveY(transform.localPosition.y + 0.1f, 1f).SetLoops(-1, LoopType.Yoyo)
         .SetEase(Ease.OutQuad);
      transform.DOLocalRotate(new Vector3(transform.localEulerAngles.x, 180f, transform.localEulerAngles.z), 2f)
         .SetLoops(-1, LoopType.Incremental)
         .SetEase(Ease.Linear);
   }

   
}
