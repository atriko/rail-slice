using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailTrigger : MonoBehaviour
{
   private bool isTriggered;

   private void OnTriggerEnter(Collider other)
   {
      var hit = other.GetComponentInParent<Player>();
      if (hit != null && !isTriggered)
      {
         isTriggered = true;
         hit.FallOff();
      }
   }
}
