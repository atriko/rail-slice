using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishGround : MonoBehaviour
{
    private bool isTriggered;
    private void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject.GetComponent<Player>();
        if (hit != null && !isTriggered)
        {
            isTriggered = true;
            hit.WinGame();
        }
    }
}
