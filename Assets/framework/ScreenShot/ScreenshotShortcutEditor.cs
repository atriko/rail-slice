#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

public class ScreenshotShortcutEditor : OdinMenuEditorWindow
{
    [MenuItem("AtRiKo/TakeScreenshots _F5")]
    public static void TakeScreenshots()
    {
        Screenshot screenshot = Resources.Load("Screenshot") as Screenshot;
        screenshot.Screenshots.TakeAllScreenshotsAtOnceIphone();
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        return null;
    }
}
#endif
