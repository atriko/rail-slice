using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Screenshot")]

public class Screenshot : SingletonScriptableObject<Screenshot>
{
    [TabGroup("Screenshots"), ShowInInspector, HideReferenceObjectPicker, HideLabel] public ScreenshotsEditor Screenshots = new ScreenshotsEditor();
}
