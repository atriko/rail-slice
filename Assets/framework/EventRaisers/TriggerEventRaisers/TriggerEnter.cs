using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.TriggerEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(TriggerEnter))]
    public class TriggerEnter : TriggerEventRaiser
    {
        private void OnTriggerEnter(Collider other)
        {
            RaiseIfTagMatches(other);
        }
    }
}