﻿using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.UnityCallbacks
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(OnDisableRaiser))]
    public class OnDisableRaiser : MonoBehaviour
    {
        [SerializeField] private UnityEvent onHappened;

        private void OnDisable()
        {
            onHappened.Invoke();
        }
    }
}