﻿using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace DHFramework.EventRaisers.UnityCallbacks
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(RandomGameEventProxy))]
    public class RandomGameEventProxy : MonoBehaviour
    {
        [Header("Parameters")] [SerializeField]
        private float delay = 0;

        [SerializeField] private int eventCountToTrigger = 1;
        [SerializeField] private bool raiseOnce = false;

        [SerializeField] private GameEvent gameEvent;

        [SerializeField] private UnityEvent onGameEventTriggered;
        private int eventRepeatCount;

        private void OnEnable()
        {
            gameEvent.AddListener(OnEventRaised);
        }

        private void OnDisable()
        {
            gameEvent.RemoveListener(OnEventRaised);
        }

        public void OnEventRaised()
        {
            if (--eventRepeatCount <= 0)
            {
                Invoke(nameof(RaiseUnityEvent), delay);

                if (raiseOnce)
                {
                    Destroy(gameObject);
                    gameEvent.RemoveListener(OnEventRaised);
                }
            }
        }

        private void RaiseUnityEvent()
        {
            int count = onGameEventTriggered.GetPersistentEventCount();
            int selectIndex = Random.Range(0, count);

            for (int i = 0; i < count; i++)
            {
                var state = selectIndex == i ? UnityEventCallState.EditorAndRuntime : UnityEventCallState.Off;
                onGameEventTriggered.SetPersistentListenerState(i, state);
            }

            onGameEventTriggered?.Invoke();
        }
    }
}