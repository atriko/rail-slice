using System;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace DHFramework.EventRaisers.SceneEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(SceneLoaded))]
    public class SceneLoaded : EventRaiser
    {
        [Serializable]
        public class Event : UnityEvent<Scene, LoadSceneMode>
        {
        }

        [SerializeField] protected Event onHappened;

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            if (arg1.Equals(LoadSceneMode.Additive))
                onHappened.Invoke(arg0, arg1);
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}