using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(EventRaiser))]
    public class EventRaiser : MonoBehaviour
    {
    }
}