﻿using UnityEngine;

namespace GameAssets.Scripts.Level
{
    [CreateAssetMenu(menuName = "Framework/Game/GameLevel", fileName = "GameLevel", order = 0)]
    public class GameLevel : DHFramework.Game.LevelSystem.Level
    {
    }
}