﻿using System;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameAssets.Scripts.Level
{
    [CreateAssetMenu(menuName = "Framework/Utilities/LevelLoader", fileName = "LevelLoader", order = 0)]
    public class LevelLoader : ScriptableObject
    {
        [SerializeField] private LevelSystem levelSystem;
        [SerializeField] private GameEvent onNextLevelLoaded;
        [SerializeField] private DHFramework.Game.Level.Zoning zoning;

        private LevelElements currentLevelElements;
        private GameLevel currentLevel;

        public GameLevel CurrentLevel => currentLevel;
        public LevelElements CurrentLevelElements => currentLevelElements;

        public void LoadNextLevel()
        {
            levelSystem.ProceedToNextLevel();
            Load();
        }

        private void Load()
        {
            SceneManager.LoadScene("Game");
        }

        public void LoadFirstLevel()
        {
            int currentLevelIndex = levelSystem.CurrentLevelIndex;

            if (!SceneManager.GetSceneByName($"Level{currentLevelIndex + 1}").isLoaded)
            {
                LoadLevelAsync(currentLevelIndex + 1, elements =>
                {
                    currentLevelElements = elements;
                    RaiseNextLevelLoaded();
                });
            }
            else
            {
                Scene scene = SceneManager.GetSceneByName($"Level{currentLevelIndex + 1}");
                currentLevelElements = scene.GetNextLevelElements(currentLevelIndex);
                RaiseNextLevelLoaded();
            }

            UpdateZoning(currentLevelIndex);
        }

        private void RaiseNextLevelLoaded()
        {
            GameLevel gameLevel = (GameLevel) levelSystem.Current;
            currentLevel = gameLevel;
            currentLevelElements.EnvironmentRoot.Init(gameLevel);
            onNextLevelLoaded.Raise();
        }

        public void RestartLevel()
        {
            Load();
        }

        private void UnloadLevel(LevelElements elements)
        {
            if (elements != null && elements.Scene.isLoaded)
            {
                SceneManager.UnloadSceneAsync(elements.Scene.name);
            }
        }

        private void LoadLevelAsync(int index, Action<LevelElements> onCompleted)
        {
            if (SceneManager.sceneCountInBuildSettings < index + 1) index = 1;
            if (SceneManager.GetSceneByName($"Level{index}").isLoaded) SceneManager.UnloadSceneAsync(index);
            LevelExtensions.LoadLevelAsync(index, scene => onCompleted.Invoke(scene.GetNextLevelElements(index)));
        }

        private void UpdateZoning(int currentLevelIndex)
        {
            zoning.ShowNextSkin();
        }

        public void LoadPreviousLevel()
        {
            levelSystem.ProceedToPreviousLevel();
            Load();
        }
    }
}