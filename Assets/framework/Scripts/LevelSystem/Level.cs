using UnityEngine;

namespace DHFramework.Game.LevelSystem
{
    [CreateAssetMenu(menuName = "Framework/Game/" + nameof(Level))]
    public class Level : ScriptableObject
    {
        [SerializeField] protected string sceneNameToLoad;

        public virtual string SceneNameToLoad => sceneNameToLoad;
    }
}