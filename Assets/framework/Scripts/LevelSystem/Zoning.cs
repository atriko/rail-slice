﻿using System;
using GameAssets.Scripts.Level;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DHFramework.Game.Level
{
    [CreateAssetMenu(menuName = "Framework/Game/" + nameof(Zoning))]
    public class Zoning : ScriptableObject
    {
        [SerializeField] private GameLevelSystem gameLevelSystem;
        [Title("Colors")] [SerializeField] private Color[] skyboxColors;
        [SerializeField] private Color[] water01Colors;
        [SerializeField] private Color[] water02Colors;
        [SerializeField] private Color[] fogColors;
        [Range(1,5),SerializeField] private int levelIndexToChangeZone = 3;

        [Title("Materials")] [SerializeField] private Material waterMat;

        private static string colorSaveKey = "_zone_color";
        private static string groundSaveKey = "_BaseColor";
        private static string waterSave01Key = "_BaseColor";
        private static string waterSave02Key = "_HorizonColor";

        [Button]
        public void ShowNextSkin()
        {
            ChangeSkin();
            if (gameLevelSystem.VirtualLevelIndexForUI % levelIndexToChangeZone == 0)
                IncreaseColorIndex();
        }

        private void ChangeSkin()
        {
            Camera cam = Camera.main;
            if (cam != null) cam.backgroundColor = skyboxColors[GetColorIndex()];
            RenderSettings.fogColor = skyboxColors[GetColorIndex()];

            // waterMat.SetColor(waterSave01Key, water01Colors[GetColorIndex()]);
            // waterMat.SetColor(waterSave02Key, water02Colors[GetColorIndex()]);

            RenderSettings.fogColor = fogColors[GetColorIndex()];
        }

        private int GetColorIndex() => PlayerPrefs.GetInt(colorSaveKey, 0);

        private void IncreaseColorIndex() => PlayerPrefs.SetInt(colorSaveKey,
            GetColorIndex() + 1 < skyboxColors.Length ? GetColorIndex() + 1 : 0);

        [Button]
        private void SaveColors(int index)
        {
            Camera cam = Camera.main;
            skyboxColors[index] = cam.backgroundColor;
            // water01Colors[index] = waterMat.GetColor(waterSave01Key);
            // water02Colors[index] = waterMat.GetColor(waterSave02Key);
            fogColors[index] = RenderSettings.fogColor;
        }
    }
}