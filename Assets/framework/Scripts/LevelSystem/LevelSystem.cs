using System;
using DHFramework.Game.LevelSystem;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Framework/Game/" + nameof(LevelSystem))]
public class LevelSystem : SerializedScriptableObject
{
    private const string levelKey = "level";
    private const string chapterKey = "chapter";
    private const string virtualLevel = "virtualLevel";

    [Serializable]
    public class SubLevel
    {
        [Title("Chapter")] [SerializeField] private Level[] levels;

        public Level[] Levels
        {
            get { return levels; }
        }
    }

    [Title("GameLevels")] [SerializeField] protected SubLevel[] levels;

    [Title("Events")] [SerializeField] protected GameEventBase onLevelsReset;
    [SerializeField] protected GameEventBase onChapterReset;
    [SerializeField] protected IntGameEvent onLevelPassed;
    [SerializeField] protected IntGameEvent onChapterPassed;

    [InfoBox("If all chapters ends, update chapter and level index.")]
    [SerializeField, PropertyRange(0, "MaxChapterIndex")]
    protected int restartChapterIndex;

    [SerializeField, PropertyRange(0, "MaxLevelIndex")]
    protected int restartLevelIndex;

    protected int currentLevelIndexInChapter;
    protected int currentChapterIndex;
    private int virtualLevelIndexForUI;

    private int MaxChapterIndex()
    {
        return levels.Length - 1;
    }

    private int MaxLevelIndex()
    {
        return levels[restartChapterIndex].Levels.Length - 1;
    }


#if UNITY_EDITOR
    [Space(20), SerializeField] private int chapter = 0;
    [SerializeField] private int level = 0;

    [Button(ButtonSizes.Medium)]
    public void LevelReset()
    {
        ResetChapter();
        ResetLevel();
    }

    [Button(ButtonSizes.Medium)]
    private void Set() => SetLevel(chapter, level);
#endif


    private void OnEnable()
    {
        CurrentLevelIndexInChapter = PlayerPrefs.GetInt(levelKey, 0);
        CurrentChapterIndex = PlayerPrefs.GetInt(chapterKey, 0);
        VirtualLevelIndexForUI = PlayerPrefs.GetInt(virtualLevel, 0);
    }

    public Level Current
    {
        get { return levels[CurrentChapterIndex].Levels[CurrentLevelIndexInChapter]; }
    }

    public virtual int LevelCountInChapter
    {
        get { return levels[currentChapterIndex].Levels.Length; }
    }

    public virtual int CurrentChapterIndex
    {
        get { return currentChapterIndex; }
        private set
        {
            currentChapterIndex = Mathf.Clamp(value, 0, levels.Length - 1);
            PlayerPrefs.SetInt(chapterKey, value);
        }
    }

    public int VirtualLevelIndexForUI
    {
        get { return virtualLevelIndexForUI; }
        private set
        {
            virtualLevelIndexForUI = value;
            PlayerPrefs.SetInt(virtualLevel, value);
        }
    }

    public virtual int CurrentLevelIndexInChapter
    {
        get { return currentLevelIndexInChapter; }
        private set
        {
            currentLevelIndexInChapter = Mathf.Clamp(value, 0, LevelCountInChapter - 1);
            PlayerPrefs.SetInt(levelKey, value);
        }
    }

    public virtual int CurrentLevelIndex
    {
        get
        {
            int level = 0;
            for (int i = 0; i < currentChapterIndex; i++)
            {
                level += levels[i].Levels.Length;
            }

            level += currentLevelIndexInChapter;


            return level;
        }
    }

    public int TotalLevelCount
    {
        get
        {
            int totalLevelCount = 0;
            for (int i = 0; i < levels.Length; i++)
            {
                totalLevelCount += levels[i].Levels.Length;
            }

            return totalLevelCount;
        }
    }

    public void ResetLevel()
    {
        CurrentLevelIndexInChapter = 0;
        VirtualLevelIndexForUI = 0;
        onLevelsReset.Raise();
    }

    public void ResetChapter()
    {
        CurrentChapterIndex = 0;

        onChapterReset.Raise();
    }

    public void SetLevel(int chapter, int level)
    {
        CurrentChapterIndex = chapter;
        CurrentLevelIndexInChapter = level;
        SetVirtualLevel();
    }

    private void SetVirtualLevel()
    {
        int lvl = 0;
        for (int i = 0; i < currentChapterIndex; i++)
        {
            lvl += levels[i].Levels.Length;
        }

        lvl += currentLevelIndexInChapter;
        VirtualLevelIndexForUI = lvl;
    }

    public void ProceedToNextLevel()
    {
        if (CurrentLevelIndexInChapter == LevelCountInChapter - 1)
        {
            if (CurrentChapterIndex == levels.Length - 1)
            {
                CurrentChapterIndex = restartChapterIndex;
                CurrentLevelIndexInChapter = restartLevelIndex;
            }
            else
            {
                CurrentChapterIndex++;
                CurrentLevelIndexInChapter = 0;
            }

            onLevelPassed.Raise(CurrentLevelIndexInChapter);
            onChapterPassed.Raise(CurrentChapterIndex);
        }
        else
        {
            CurrentLevelIndexInChapter++;

            onLevelPassed.Raise(CurrentLevelIndexInChapter);
        }

        VirtualLevelIndexForUI++;
    }

    public void ProceedToPreviousLevel()
    {
        if (CurrentLevelIndexInChapter == 0)
        {
            if (CurrentChapterIndex == 0)
            {
                CurrentChapterIndex = restartChapterIndex;
                CurrentLevelIndexInChapter = restartLevelIndex;
            }
            else
            {
                CurrentChapterIndex--;
                CurrentLevelIndexInChapter = LevelCountInChapter - 1;
            }
        }
        else
        {
            CurrentLevelIndexInChapter--;
        }

        VirtualLevelIndexForUI--;
    }
}