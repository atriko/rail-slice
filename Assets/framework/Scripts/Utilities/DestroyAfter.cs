using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.Game.Utilities
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Game/" + nameof(DestroyAfter))]
    public class DestroyAfter : MonoBehaviour
    {
        [SerializeField] private float delay;

        private void Start()
        {
            Destroy(gameObject, delay);
        }
    }
}