using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.Game.Utilities
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Game/" + nameof(DestroyGameObject))]
    public class DestroyGameObject : MonoBehaviour
    {
        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}