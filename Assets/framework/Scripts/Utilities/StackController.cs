using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts;
using Lean.Touch;
using UniRx;
using UnityEngine;

public class StackController : MonoBehaviour
{
    [SerializeField] private Transform playerModel;

    [Header("Stack Settings")] [SerializeField]
    private float frontStackOffset;

    [SerializeField] private float backStackOffset;
    [SerializeField] private float startStackCount;

    [Header("Player Settings")] [SerializeField]
    private float maxXPos;

    [SerializeField] private float minXPos;
    [SerializeField] private float forwardSpeed;
    [SerializeField] private float sensitivity;

    [Header("Wave Settings")] [SerializeField]
    private float waveFrequency;

    [SerializeField] private float wavePunchScale;

    private float currentFrontStackOffset;
    private float currentBackStackOffset;

    private Vector3 targetPosition;
    private Vector2 screenDelta;
    private List<LeanFinger> fingers;
    private LeanFingerFilter use = new LeanFingerFilter(true);

    private List<Transform> frontStack = new List<Transform>();
    private List<Transform> backStack = new List<Transform>();
    private List<Tween> scalePunches = new List<Tween>();

    private IDisposable touchDisposable;
    private IDisposable horizontalMovement;
    private IDisposable forwardMovement;

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        EventManager.Instance.SubscribeEvent(GameEventType.GameStart,LevelStart);
        frontStack.Add(playerModel);
        backStack.Add(playerModel);
        for (int i = 0; i < startStackCount; i++)
        {
            CollectBackStack(PoolManager.Instance.GetFromPool("Domino",Vector3.zero, Quaternion.identity).transform);
        }
    }

    private void LevelStart()
    {
        touchDisposable = Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(ListenTouch);
        horizontalMovement = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(ControlStacks);
        forwardMovement = Observable.EveryFixedUpdate().TakeUntilDisable(this).Subscribe(MoveForward);
        Observable.Interval(TimeSpan.FromSeconds(0.5f)).TakeUntilDisable(this).Subscribe(delegate(long l)
        {
            CollectBackStack(PoolManager.Instance.GetFromPool("Domino",Vector3.zero, Quaternion.identity).transform);

        });  
    }

    private void ListenTouch(long obj)
    {
        fingers = use.UpdateAndGetFingers();
        screenDelta = LeanGesture.GetScaledDelta(fingers);
    }

    private void MoveForward(long obj)
    {
        transform.position += Vector3.forward * forwardSpeed / 10;
    }

    private void ControlStacks(long obj)
    {
        targetPosition += Vector3.right * (screenDelta.x * sensitivity / 10f) / 100f;
        var clampedX = Mathf.Clamp(targetPosition.x, minXPos, maxXPos);
        targetPosition.x = clampedX;
        frontStack[0].transform.localPosition = Vector3.Lerp(frontStack[0].transform.localPosition, targetPosition,
            Time.fixedDeltaTime * 7.5f);
        for (var i = 1; i < frontStack.Count; i++)
        {
            var pos = frontStack[i].transform.localPosition;
            pos.x = Mathf.Lerp(frontStack[i].transform.localPosition.x, frontStack[i - 1].transform.localPosition.x,
                Time.fixedDeltaTime * (Mathf.Pow(2, i / 10f) + 14));
            frontStack[i].transform.localPosition = pos;
        }

        for (var i = 1; i < backStack.Count; i++)
        {
            var pos = backStack[i].transform.localPosition;
            pos.x = Mathf.Lerp(backStack[i].transform.localPosition.x, backStack[i - 1].transform.localPosition.x,
                Time.fixedDeltaTime * (Mathf.Pow(2, i / 10f) + 14));
            backStack[i].transform.localPosition = pos;
        }
    }

    public void CollectFrontStack(Transform stack)
    {
        HapticManager.Instance.HapticFeedback(HapticType.Light);
        frontStack.Add(stack);
        stack.transform.SetParent(transform);
        currentFrontStackOffset += frontStackOffset;
        stack.transform.localPosition = frontStack[0].localPosition + new Vector3(0, 0, currentFrontStackOffset);
        CreateWave(frontStack);
    }

    public void CollectBackStack(Transform stack)
    {
        HapticManager.Instance.HapticFeedback(HapticType.Light);
        backStack.Add(stack);
        stack.transform.SetParent(transform);
        currentBackStackOffset -= backStackOffset;
        stack.transform.localPosition = backStack[0].localPosition + new Vector3(0, 0, currentBackStackOffset);
        CreateWave(backStack);
    }

    private void CreateWave(List<Transform> stack)
    {
        int i = stack.Count - 1;
        Observable.Interval(TimeSpan.FromSeconds(waveFrequency)).Take(stack.Count - 1).Subscribe(delegate
        {
            var currentStack = stack[i];
            var sameTween = scalePunches.Find(tween1 => (Transform)tween1.target == currentStack);
            if (sameTween != null)
            {
                scalePunches.Remove(sameTween);
                sameTween.Kill(true);
            }

            var tween = currentStack.DOPunchScale(currentStack.localScale * wavePunchScale, 0.2f, 4);
            scalePunches.Add(tween);
            i--;
        });
    }

    public void RemoveFromFrontStack(Transform removedStack, bool killRemaining = false)
    {
        if (!frontStack.Contains(removedStack)) return;
        if (killRemaining)
        {
            var cartIndex = frontStack.IndexOf(removedStack);
            if (cartIndex == 0)
            {
                return;
            }

            frontStack.GetRange(cartIndex, frontStack.Count - cartIndex).ForEach(stack =>
            {
                stack.transform.SetParent(null);
                currentFrontStackOffset -= frontStackOffset;
            });
            frontStack.RemoveRange(cartIndex, frontStack.Count - cartIndex);
        }
        else
        {
            removedStack.transform.SetParent(null);
            currentFrontStackOffset -= frontStackOffset;
            frontStack.Remove(removedStack);
        }
    }

    public void RemoveFromBackStack(Transform removedStack, bool killRemaining = false)
    {
        if (!backStack.Contains(removedStack)) return;
        if (killRemaining)
        {
            var cartIndex = backStack.IndexOf(removedStack);
            if (cartIndex == 0)
            {
                return;
            }

            backStack.GetRange(cartIndex, backStack.Count - cartIndex).ForEach(stack =>
            {
                stack.transform.SetParent(null);
                currentBackStackOffset += backStackOffset;
            });
            backStack.RemoveRange(cartIndex, backStack.Count - cartIndex);
        }
        else
        {
            removedStack.transform.SetParent(null);
            currentBackStackOffset += backStackOffset;
            backStack.Remove(removedStack);
        }
    }

    public void DisableControls()
    {
        forwardMovement?.Dispose();
        horizontalMovement?.Dispose();
        touchDisposable?.Dispose();
    }
}
