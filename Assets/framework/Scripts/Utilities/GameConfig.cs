﻿using UnityEngine;

namespace GameAssets.Scripts
{
    [CreateAssetMenu(fileName = "GameConfigs", menuName = "Game/Configs")]
    public class GameConfig : SingletonScriptableObject<GameConfig>
    {
        [Header("Player")] public float forwardSpeed;
        public float sensitivity;
        public float rotationMaxAngle;
        public float rotationSmoothness;

        [Header("RagdollSettings")] public float ragdollForwardForce;
        public float ragdollUpwardForce;

        [Header("Environment")] public float minBuildingHeight;
        public float maxBuildingHeight;

        [Header("Fruits")] public float fruitForwardMinCutPower;
        public float fruitForwardMaxCutPower;
        public float fruitUpwardsMinCutPower;
        public float fruitUpwardsMaxCutPower;

        [Header("BladeSettings")] public float reCenterDelay;
        public Material bladeMaterial;
        public float bladeGrowAmount;
        public float bladeLoseAmount;
        public float bladeLoseInterval;
        public float cutPieceForwardLaunch;
        public float cutPieceUpwardsLaunch;
        
        [Header("UISettings")]
        public Vector3 moneyPopupOffset;
    }
}