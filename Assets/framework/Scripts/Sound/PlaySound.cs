using ScriptableObjectArchitecture;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DHFramework.Game.Sound
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Sound/" + nameof(PlaySound))]
    public class PlaySound : MonoBehaviour
    {
        [SerializeField] private AudioClip[] sounds;
        [SerializeField] private AudioSource source;

        private const string soundKey = "sound-enabled";

        private void Awake()
        {
            if (IsActive()) EnableSound();
            else DisableSound();
        }

        public void PlayRandom() => source.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
        public void PlaySoundAtIndex(int index) => source.PlayOneShot(sounds[index]);
        public void PlaySoundByClip(AudioClip clip) => source.PlayOneShot(clip);
        
        public void EnableSound()
        {
            source.mute = false;
            PlayerPrefs.SetInt(soundKey, 0);
        }

        public void DisableSound()
        {
            source.mute = true;
            PlayerPrefs.SetInt(soundKey, 1);
        }

        public bool IsActive() => PlayerPrefs.GetInt(soundKey, 0) == 0;
    }
}