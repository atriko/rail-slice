using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : PanelBase
{
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private Image fillBar;

    private IDisposable fillBarDisposable;
    private float maxDistance;
    public InGamePanel()
    {
        Type = PanelType.InGame;
    }
    private void Start()
    {
        maxDistance = Vector3.Distance(Player.Instance.transform.position, FinishTrigger.Instance.transform.position);
    }
    private void OnEnable()
    {
        levelText.text = $"Level {LevelManager.Instance.GetCurrentLevel()}";
        fillBarDisposable = Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(delegate(long l)
        {
            var distance = 1 - Vector3.Distance(Player.Instance.transform.position, FinishTrigger.Instance.transform.position) / maxDistance;
            fillBar.fillAmount = distance;
            if (distance >= 0.98f)
            {
                fillBar.fillAmount = 1f;
                fillBarDisposable?.Dispose(); 
            }
        });
    }

    public void RestartLevel()
    {
        LevelManager.Instance.RestartLevel();
    }
}