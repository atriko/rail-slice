using DG.Tweening;
using UnityEngine;

public class SuccessPanel : PanelBase
{
    [SerializeField] private DOTweenAnimation fadeAnimation;
    
    public SuccessPanel()
    {
        Type = PanelType.Success;
    }
    public void ContinueButtonClicked()
    {
        fadeAnimation.DOPlay();
    }

    public void OnFadeComplete()
    {
        LevelManager.Instance.LoadNextLevel();
    }
}