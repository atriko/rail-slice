using DG.Tweening;
using Lean.Touch;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanel : PanelBase
{
    [SerializeField] private DOTweenAnimation fadeAnimation;

    public MainMenuPanel()
    {
        Type = PanelType.Main;
    }

    private void Awake()
    {
        EventManager.Instance.SubscribeEvent(GameEventType.LevelLoaded, () => fadeAnimation.DOPlay());
    }

    private void OnEnable()
    {
        LeanTouch.OnFingerDown += OnFingerDown;
        GetComponent<Image>().enabled = true;
    }

    private void OnFingerDown(LeanFinger obj)
    {
        GameManager.Instance.StartGame();
    }

    private void OnDisable()
    {
        LeanTouch.OnFingerDown -= OnFingerDown;
    }
}