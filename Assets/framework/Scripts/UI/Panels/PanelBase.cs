using System;
using UnityEngine;

public abstract class PanelBase : MonoBehaviour
{
    public PanelType Type { get; protected set; }

}
