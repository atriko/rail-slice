using DG.Tweening;
using UnityEngine;

public class FailPanel : PanelBase
{
    [SerializeField] private DOTweenAnimation fadeAnimation;

    public FailPanel()
    {
        Type = PanelType.Fail;
    }
    public void RestartButtonClicked()
    {
        fadeAnimation.DOPlay();
    }
    public void OnFadeComplete()
    {
        LevelManager.Instance.RestartLevel();
    }
}
