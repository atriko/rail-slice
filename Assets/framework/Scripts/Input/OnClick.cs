using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.Game.Input
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Input/" + nameof(OnClick))]
    public class OnClick : MonoBehaviour
    {
        [SerializeField] private UnityEvent onClicked;
        
        private void OnMouseDown()
        {
            onClicked.Invoke();
        }
    }
}