using System;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjectArchitecture;
using UnityEngine;

public enum GameEventType
{
    GameStart,
    GameWon,
    GameLost,
    GameEnd,
    LevelLoaded
}

[Serializable]
public struct GameEvents
{
    public GameEventBase gameEvent;
    public GameEventType eventType;
}
public class EventManager : MonoSingleton<EventManager>
{
    [SerializeField] private List<GameEvents> events;
    private List<Action> subscribedEvents = new List<Action>();
    
    public GameEventBase GetEvent(GameEventType type)
    {
        return events.First(e => e.eventType == type).gameEvent;
    }
    public void RaiseEvent(GameEventType type)
    {
        GetEvent(type).Raise();
    }
    public void SubscribeEvent(GameEventType type, Action action)
    {
        GetEvent(type).AddListener(action);
        subscribedEvents.Add(action);
    }
    public void UnsubscribeEvent(GameEventType type, Action action)
    {
        GetEvent(type).RemoveListener(action);
    }
    private void OnDisable()
    {
        UnsubscribeAll();
    }
    private void UnsubscribeAll()
    {
        foreach (var e in events)
        {
            foreach (var subscribedEvent in subscribedEvents)
            {
                UnsubscribeEvent(e.eventType, subscribedEvent);
            
            }
        }
        
    }
}