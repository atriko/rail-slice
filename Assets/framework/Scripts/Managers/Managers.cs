﻿using UnityEngine;

namespace DHFramework.Game
{
    [CreateAssetMenu(menuName = "Framework/Game/" + nameof(Managers))]
    public class Managers : ScriptableObject
    {
        [SerializeField] private ScriptableObject[] managers;
        public ScriptableObject[] GetManagers()
        {
            return managers;
        }
    }
}