﻿// using MoreMountains.NiceVibrations;
using UnityEngine;

namespace GameAssets.Scripts
{
    public enum HapticType { Light, Medium, Heavy, Success, Failure }
    public class HapticManager : MonoSingleton<HapticManager>
    {
        private bool isHapticOn;
        private const string HAPTIC_KEY = "Haptic";
        private bool IsHapticOn
        {
            get
            {
                isHapticOn = PlayerPrefs.GetInt(HAPTIC_KEY, 1) == 1;
                return isHapticOn;
            }
            set
            {
                isHapticOn = value;
                PlayerPrefs.SetInt(HAPTIC_KEY, value ? 1 : 0);
            }
        }

        public void HapticFeedback(HapticType type)
        {
            // if (!IsHapticOn) return;
            // HapticTypes hapticFeedback = HapticTypes.LightImpact;
            // switch (type)
            // {
            //     case HapticType.Light:
            //         hapticFeedback = HapticTypes.LightImpact;
            //         break;
            //     case HapticType.Medium:
            //         hapticFeedback = HapticTypes.MediumImpact;
            //         break;
            //     case HapticType.Heavy:
            //         hapticFeedback = HapticTypes.HeavyImpact;
            //         break;
            //     case HapticType.Success:
            //         hapticFeedback = HapticTypes.Success;
            //         break;
            //     case HapticType.Failure:
            //         hapticFeedback = HapticTypes.Failure;
            //         break;
            // }
            // MMVibrationManager.Haptic(hapticFeedback, false, true, this);
        }

        public void EnableHaptic() => IsHapticOn = true;
        public void DisableHaptic() => IsHapticOn = false;
        public bool GetHapticStatus() => IsHapticOn;
    }
}