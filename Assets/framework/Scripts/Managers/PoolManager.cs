using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts
{
    public class PoolManager : MonoSingleton<PoolManager>
    {
        [Serializable]
        public class Pool
        {
            [FoldoutGroup("PooledObject")] public string Name;
            [FoldoutGroup("PooledObject")] public int Size = 1;
            [FoldoutGroup("PooledObject")] public GameObject Prefab;
        }

        [SerializeField] private Pool[] pools;
        [SerializeField] private List<GameObject>[] pooledItems;
        [SerializeField] private int defaultPoolAmount = 10;

        private void Awake()
        {
            pooledItems = new List<GameObject>[pools.Length];

            for (int i = 0; i < pools.Length; i++)
            {
                pooledItems[i] = new List<GameObject>();

                int poolingAmount;
                if (pools[i].Size > 0) poolingAmount = pools[i].Size;
                else poolingAmount = defaultPoolAmount;

                for (int j = 0; j < poolingAmount; j++)
                {
                    GameObject newItem = Instantiate(pools[i].Prefab);
                    newItem.SetActive(false);
                    pooledItems[i].Add(newItem);
                    newItem.transform.parent = transform;
                }
            }
        }

        public void ReturnToPool(GameObject poolObject, float duration = 0f)
        {
            Observable.Timer(TimeSpan.FromSeconds(duration)).Subscribe(_ =>
            {
                poolObject.transform.SetParent(transform);
                poolObject.SetActive(false);
            }).AddTo(gameObject);
        }

        public GameObject GetFromPool(string name)
        {
            GameObject newObject = GetPooledItem(name);
            if (newObject != null)
            {
                newObject.SetActive(true);
                return newObject;
            }

            return null;
        }

        public GameObject GetFromPool(string name, Vector3 itemPosition, Quaternion itemRotation)
        {
            GameObject newObject = GetPooledItem(name);
            if (newObject != null)
            {
                newObject.transform.position = itemPosition;
                newObject.transform.rotation = itemRotation;
                newObject.SetActive(true);
                return newObject;
            }

            return null;
        }

        public GameObject GetFromPool(string name, Vector3 itemPosition)
        {
            GameObject newObject = GetPooledItem(name);
            if (newObject != null)
            {
                newObject.transform.position = itemPosition;
                newObject.SetActive(true);
                return newObject;
            }

            return null;
        }

        public GameObject GetFromPool(string name, Vector3 itemPosition, Quaternion itemRotation,
            GameObject myParent = null)
        {
            GameObject newObject = GetPooledItem(name);
            if (newObject != null)
            {
                newObject.transform.position = itemPosition;
                newObject.transform.rotation = itemRotation;
                newObject.transform.parent = myParent.transform;
                newObject.SetActive(true);
                return newObject;
            }

            return null;
        }

        private GameObject GetPooledItem(string name)
        {
            for (int i = 0; i < pools.Length; i++)
            {
                if (pools[i].Name == name)
                {
                    for (int j = 0; j < pooledItems[i].Count; j++)
                    {
                        if (!pooledItems[i][j].activeInHierarchy)
                        {
                            return pooledItems[i][j];
                        }
                    }

                    GameObject newItem = Instantiate(pools[i].Prefab);
                    newItem.SetActive(false);
                    pooledItems[i].Add(newItem);
                    newItem.transform.parent = transform;
                    return newItem;
                }
            }

            return null;
        }
    }
}