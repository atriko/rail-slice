using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Framework/Game/" + nameof(GameManager))]
public class GameManager : SingletonScriptableObject<GameManager>
{
    private bool isGameEnded;
    public bool IsGameEnded => isGameEnded;

    [Title("Actions")]
    [Button(ButtonSizes.Large), GUIColor(1f, 0.5f, 0f)]
    public void StartGame()
    {
        EventManager.Instance.RaiseEvent(GameEventType.GameStart);
        isGameEnded = false;
    }

    [Button(ButtonSizes.Large), GUIColor(1f, 0.2f, 0f)]
    public void LoseGame()
    {
        EventManager.Instance.RaiseEvent(GameEventType.GameLost);
        EventManager.Instance.RaiseEvent(GameEventType.GameEnd);
        isGameEnded = true;
    }

    [Button(ButtonSizes.Large), GUIColor(0f, 1f, 0f)]
    public void WinGame()
    {
        EventManager.Instance.RaiseEvent(GameEventType.GameWon);
        EventManager.Instance.RaiseEvent(GameEventType.GameEnd);
        isGameEnded = true;
    }

    public static float Remap(float a1, float b1, float a2, float b2, float value)
    {
        float t = Mathf.InverseLerp(a1, b1, value);
        return Mathf.Lerp(a2, b2, t);
    }
}