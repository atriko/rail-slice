﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Framework/Game/" + nameof(TutorialManager))]
public class TutorialManager : ScriptableObject
{
    [SerializeField] private LevelSystem levelSystem;
    [SerializeField] private TutorialData[] tutorialData;

    public void Show()
    {
        int levelIndex = levelSystem.CurrentLevelIndex;
        for (int index = 0; index < tutorialData.Length; index++)
        {
            TutorialData data = tutorialData[index];
            if (levelIndex == data.Level)
            {
                data.Show();
            }
        }
    }

    [Button(ButtonSizes.Large), GUIColor(1f, 0.5f, 0f)]
    public void ResetTutorials()
    {
        for (int index = 0; index < tutorialData.Length; index++)
        {
            PlayerPrefs.DeleteKey(tutorialData[index].TutorialName);
        }
    }
}

[Serializable]
public class TutorialData
{
    [SerializeField] private int level;
    [SerializeField] private string tutorialName;
    [SerializeField] private UnityEvent onTutorialShowed;
    public int Level => level;
    public string TutorialName => "tutorial_" + tutorialName;

    [Button(ButtonSizes.Small), GUIColor(0f, 1f, 0f)]
    public void Show()
    {
        bool isShowed = PlayerPrefs.GetInt(TutorialName, 0) == 1;
        if (!isShowed)
        {
            onTutorialShowed?.Invoke();
            PlayerPrefs.SetInt(TutorialName, 1);
        }
    }
}