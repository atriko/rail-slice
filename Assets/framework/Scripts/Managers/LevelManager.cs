using GameAssets.Scripts.Level;
using UnityEngine;

public class LevelManager : MonoSingleton<LevelManager>
{
   [SerializeField] private LevelLoader levelLoader;
   [SerializeField] private GameLevelSystem levelSystem;
   
   private void Start()
   {
      levelLoader.LoadFirstLevel();
   }
   public void LoadNextLevel()
   {
      levelLoader.LoadNextLevel();
   }
   public void RestartLevel()
   {
      levelLoader.RestartLevel();
   }
   public string GetCurrentLevel()
   {
      return (levelSystem.VirtualLevelIndexForUI + 1).ToString();
   }
}

