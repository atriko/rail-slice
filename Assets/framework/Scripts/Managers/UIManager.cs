using System.Collections.Generic;
using System.Linq;
using Framework.Game.Currency;
using GameAssets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public enum PanelType
{
    Main,
    InGame,
    Success,
    Fail
}

public class UIManager : MonoSingleton<UIManager>
{
    private List<PanelBase> allPanels;

    private void Awake()
    {
        allPanels = GetComponentsInChildren<PanelBase>(true).ToList();
        EventManager.Instance.SubscribeEvent(GameEventType.GameStart, OnGameStarted);
        EventManager.Instance.SubscribeEvent(GameEventType.GameWon, OnGameWon);
        EventManager.Instance.SubscribeEvent(GameEventType.GameLost, OnGameLost);
    }

    private void Start()
    {
        ShowPanel(PanelType.Main);
    }
    private void OnGameStarted()
    {
        ShowPanel(PanelType.InGame);
    }

    private void OnGameWon()
    {
        ShowPanel(PanelType.Success);
    }

    private void OnGameLost()
    {
        ShowPanel(PanelType.Fail);
    }

    public void ShowPanelAdditive(PanelType type)
    {
        var panel = allPanels.First(x => x.Type == type);
        if (panel != null)
        {
            panel.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Panel not found");
        }
    }

    public PanelBase GetPanel(PanelType type)
    {
        return allPanels.Find(panel => panel.Type.Equals(type));
    }
    
    public void ShowPanel(PanelType type)
    {
        HideAllPanels();
        allPanels.First(x => x.Type == type).gameObject.SetActive(true);
    }

    public void HidePanel(PanelType type)
    {
        allPanels.First(x => x.Type == type).gameObject.SetActive(false);
    }

    public void HideAllPanels()
    {
        foreach (var panel in allPanels)
        {
            panel.gameObject.SetActive(false);
        }
    }
    
    public string RankToText(int rank)
    {
        switch (rank % 100)
        {
            case 11:
            case 12:
            case 13:
                return  rank + "th";
        }

        switch (rank % 10)
        {
            case 1:
                return  rank + "st";
            case 2:
                return  rank + "nd";
            case 3:
                return  rank + "rd";
            default:
                return  rank + "th";
        }
    }
    
    public void PopupMoney(Vector3 position,int amount)
    {
        var popup = PoolManager.Instance.GetFromPool("Popup", position + GameConfig.Instance.moneyPopupOffset);
        popup.GetComponentInChildren<TextMeshProUGUI>().text = $"+{amount}";
        Currency.Instance.SetCurrency(Currency.Instance.CurrentCurrency + amount);
    }
}