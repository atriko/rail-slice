using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DHFramework.Plugins.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Plugins.Editor
{
    public enum Phase
    {
        Setup,
        Processing,
        Complete
    };

    public enum SearchedType
    {
        Asset,
        SceneObject,
        Mixed
    };

    public class AssetUsagePanel : EditorWindow
    {
        private static AssetUsagePanel window;

        // [MenuItem("DHFramework/" +
        //           "Asset Finder %&s")]
        // public new static void Show()
        // {
        //     window = GetWindow<AssetUsagePanel>("Asset Finder");
        // }

        public static GUILayoutOption GL_EXPAND_WIDTH = GUILayout.ExpandWidth(true);
        public static GUILayoutOption GL_EXPAND_HEIGHT = GUILayout.ExpandHeight(true);
        public static GUILayoutOption GL_WIDTH_25 = GUILayout.Width(25);
        public static GUILayoutOption GL_HEIGHT_30 = GUILayout.Height(30);
        public static GUILayoutOption GL_HEIGHT_40 = GUILayout.Height(40);

        private static GUIStyle m_boxGUIStyle;

        public static GUIStyle BoxGUIStyle
        {
            get
            {
                if (m_boxGUIStyle == null)
                {
                    m_boxGUIStyle = new GUIStyle(EditorStyles.helpBox)
                    {
                        alignment = TextAnchor.MiddleCenter,
                        font = EditorStyles.label.font
                    };
                }

                return m_boxGUIStyle;
            }
        }

        private List<Object> assetsToSearch = new List<Object>() {null};
        private List<SubAssetToSearch> subAssetsToSearch = new List<SubAssetToSearch>();

        private HashSet<Object> assetsSet;
        private Type[] assetClasses;
        private Phase currentPhase = Phase.Setup;

        private List<ReferenceHolder> searchResult = new List<ReferenceHolder>();
        private ReferenceHolder currentReferenceHolder;
        private Dictionary<Type, VariableGetterHolder[]> typeToVariables;
        private Dictionary<Type, bool> searchableTypes;
        private Dictionary<string, Node> searchedObjects;
        private Stack<object> callStack;

        private Stack<Type> searchedTypesStack;

        private bool searchMaterialAssets;
        private bool searchGameObjectReferences;
        private bool searchMonoBehavioursForScript;
        private bool searchRenderers;
        private bool searchMaterialsForShader;
        private bool searchMaterialsForTexture;

        private bool searchSerializableVariablesOnly;
        private bool showSubAssetsFoldout = true;

        private SearchedType searchedType = SearchedType.Mixed;

        private int searchDepthLimit = 3;
        private int currentDepth = 3;

        private SceneSetup[] initialSceneSetup;

        private BindingFlags fieldModifiers = BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public |
                                              BindingFlags.NonPublic;

        private BindingFlags propertyModifiers = BindingFlags.Instance | BindingFlags.DeclaredOnly |
                                                 BindingFlags.Public |
                                                 BindingFlags.NonPublic;

        private int prevSearchDepthLimit;
        private BindingFlags prevFieldModifiers;
        private BindingFlags prevPropertyModifiers;

        private const float PLAY_MODE_REFRESH_INTERVAL = 1f;
        private double nextPlayModeRefreshTime = 0f;


        private static GUIStyle m_tooltipGUIStyle;
        private Vector2 scrollPosition = Vector2.zero;

        private List<Node> nodesPool = new List<Node>(32);
        private List<VariableGetterHolder> validVariables = new List<VariableGetterHolder>(32);

        private AssetFinder assetFinder;

        private void OnEnable()
        {
            window = this;

            assetFinder = new AssetFinder(window);
            assetFinder.StartSearchEvent += StartSearch;
        }

        void Update()
        {
            if (EditorApplication.isPlaying && currentPhase == Phase.Complete &&
                EditorApplication.timeSinceStartup >= nextPlayModeRefreshTime)
            {
                nextPlayModeRefreshTime = EditorApplication.timeSinceStartup + PLAY_MODE_REFRESH_INTERVAL;
                Repaint();
            }
        }

        void OnGUI()
        {
            if (currentPhase == Phase.Setup) assetFinder.ShowInputMode();

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GL_EXPAND_WIDTH, GL_EXPAND_HEIGHT);

            GUILayout.BeginVertical();

            if (currentPhase == Phase.Processing)
            {
                this.ShowNotification(new GUIContent("    SOMETHING WENT WRONG, RESTART PLEASE!    "));
                currentPhase = Phase.Setup;
            }
            else if (currentPhase == Phase.Setup)
            {
                if (ExposeSearchedAssets()) OnSearchedAssetsChanged();

                if (subAssetsToSearch.Count > 0)
                {
                    if (showSubAssetsFoldout)
                    {
                        for (int i = 0; i < subAssetsToSearch.Count; i++)
                        {
                            GUILayout.BeginHorizontal();

                            subAssetsToSearch[i].shouldSearch =
                                EditorGUILayout.Toggle(subAssetsToSearch[i].shouldSearch, GL_WIDTH_25);

                            GUI.enabled = false;
                            EditorGUILayout.ObjectField(string.Empty, subAssetsToSearch[i].subAsset, typeof(Object),
                                true);
                            GUI.enabled = true;

                            GUILayout.EndHorizontal();
                        }
                    }
                }
            }
            else if (currentPhase == Phase.Complete)
            {
                GUI.enabled = false;

                ExposeSearchedAssets();

                GUILayout.Space(10);
                GUI.enabled = true;

                GUI.color = Color.red;

                if (GUILayout.Button("RESET SEARCH", BoxGUIStyle, GL_EXPAND_WIDTH, GL_HEIGHT_40))
                {
                    currentPhase = Phase.Setup;
                    if (searchResult != null) searchResult.Clear();
                }

                if (searchResult.Count == 0)
                {
                    GUILayout.Box("No results found...", GL_EXPAND_WIDTH);
                }
                else
                {
                    for (int i = 0; i < searchResult.Count; i++) searchResult[i].DrawOnGUI();
                }

                GUI.color = Color.white;
            }


            GUILayout.Space(10);
            GUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }

        private void StartSearch(Object obj)
        {
            assetsToSearch = new List<Object>();
            assetsToSearch.Add(obj);

            currentPhase = Phase.Processing;
            initialSceneSetup = !EditorApplication.isPlaying ? EditorSceneManager.GetSceneManagerSetup() : null;
            ExecuteQuery();
        }

        private bool ExposeSearchedAssets()
        {
            bool hasChanged = false;
            Event ev = Event.current;

            GUILayout.BeginHorizontal();


            if (GUI.enabled)
            {
                if ((ev.type == EventType.DragPerform || ev.type == EventType.DragUpdated) &&
                    GUILayoutUtility.GetLastRect().Contains(ev.mousePosition))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    if (ev.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        Object[] draggedObjects = DragAndDrop.objectReferences;
                        if (draggedObjects.Length > 0)
                        {
                            for (int i = 0; i < draggedObjects.Length; i++)
                            {
                                if (draggedObjects[i] != null && !draggedObjects[i].Equals(null))
                                {
                                    hasChanged = true;
                                    assetsToSearch.Add(draggedObjects[i]);
                                }
                            }
                        }
                    }

                    ev.Use();
                }
            }

            GUILayout.EndHorizontal();

            return hasChanged;
        }

        private void OnSearchedAssetsChanged()
        {
            subAssetsToSearch.Clear();

            if (AreSearchedAssetsEmpty()) return;

            MonoScript[] monoScriptsInProject = null;
            HashSet<Object> currentSubAssets = new HashSet<Object>();
            for (int i = 0; i < assetsToSearch.Count; i++)
            {
                Object assetToSearch = assetsToSearch[i];
                if (assetToSearch == null || assetToSearch.Equals(null)) continue;

                if (!assetToSearch.IsAsset() || !AssetDatabase.IsMainAsset(assetToSearch) ||
                    assetToSearch is SceneAsset)
                    continue;

                Object[] assets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(assetToSearch));
                for (int j = 0; j < assets.Length; j++)
                {
                    Object asset = assets[j];
                    if (asset == null || asset.Equals(null) || asset is Component) continue;

                    if (currentSubAssets.Contains(asset)) continue;

                    if (asset != assetToSearch)
                    {
                        subAssetsToSearch.Add(new SubAssetToSearch(asset, true));
                        currentSubAssets.Add(asset);
                    }

                    if (asset is MonoScript)
                    {
                        Type monoScriptType = ((MonoScript) asset).GetClass();
                        if (monoScriptType == null ||
                            (!monoScriptType.IsInterface && !typeof(Component).IsAssignableFrom(monoScriptType)))
                            continue;

                        if (monoScriptsInProject == null)
                        {
                            string[] pathsToMonoScripts = AssetDatabase.FindAssets("t:MonoScript");
                            monoScriptsInProject = new MonoScript[pathsToMonoScripts.Length];
                            for (int k = 0; k < pathsToMonoScripts.Length; k++)
                                monoScriptsInProject[k] =
                                    AssetDatabase.LoadAssetAtPath<MonoScript>(
                                        AssetDatabase.GUIDToAssetPath(pathsToMonoScripts[k]));
                        }

                        for (int k = 0; k < monoScriptsInProject.Length; k++)
                        {
                            Type otherMonoScriptType = monoScriptsInProject[k].GetClass();
                            if (otherMonoScriptType == null || monoScriptType == otherMonoScriptType ||
                                !monoScriptType.IsAssignableFrom(otherMonoScriptType))
                                continue;

                            if (!currentSubAssets.Contains(monoScriptsInProject[k]))
                            {
                                subAssetsToSearch.Add(new SubAssetToSearch(monoScriptsInProject[k], false));
                                currentSubAssets.Add(monoScriptsInProject[k]);
                            }
                        }
                    }
                }
            }
        }

        private bool AreSearchedAssetsEmpty()
        {
            for (int i = 0; i < assetsToSearch.Count; i++)
            {
                if (assetsToSearch[i] != null && !assetsToSearch[i].Equals(null)) return false;
            }

            return true;
        }

        private void ExecuteQuery()
        {
            if (searchResult == null) searchResult = new List<ReferenceHolder>(16);
            else searchResult.Clear();

            if (typeToVariables == null) typeToVariables = new Dictionary<Type, VariableGetterHolder[]>(4096);
            else if (searchDepthLimit != prevSearchDepthLimit || prevFieldModifiers != fieldModifiers ||
                     prevPropertyModifiers != propertyModifiers) typeToVariables.Clear();

            if (searchableTypes == null) searchableTypes = new Dictionary<Type, bool>(4096);
            else searchableTypes.Clear();

            if (searchedObjects == null) searchedObjects = new Dictionary<string, Node>(32768);
            else searchedObjects.Clear();

            if (callStack == null) callStack = new Stack<object>(64);
            else callStack.Clear();

            if (searchedTypesStack == null) searchedTypesStack = new Stack<Type>(8);
            if (assetsSet == null) assetsSet = new HashSet<Object>();
            else assetsSet.Clear();

            prevSearchDepthLimit = searchDepthLimit;
            prevFieldModifiers = fieldModifiers;
            prevPropertyModifiers = propertyModifiers;

            HashSet<Type> allAssetClasses = new HashSet<Type>();

            searchMaterialAssets = false;
            searchGameObjectReferences = false;
            searchMonoBehavioursForScript = false;
            searchRenderers = false;
            searchMaterialsForShader = false;
            searchMaterialsForTexture = false;

            for (int i = 0; i < assetsToSearch.Count; i++)
            {
                bool isAsset = assetsToSearch[i].IsAsset();
                if (i == 0) searchedType = isAsset ? SearchedType.Asset : SearchedType.SceneObject;
                else if (searchedType != SearchedType.Mixed)
                {
                    if (isAsset && searchedType == SearchedType.SceneObject) searchedType = SearchedType.Mixed;
                    else if (!isAsset && searchedType == SearchedType.Asset) searchedType = SearchedType.Mixed;
                }
            }

            try
            {
                for (int i = 0; i < assetsToSearch.Count; i++)
                    subAssetsToSearch.Add(new SubAssetToSearch(assetsToSearch[i], true));

                for (int i = 0; i < subAssetsToSearch.Count; i++)
                {
                    if (subAssetsToSearch[i].shouldSearch)
                    {
                        Object asset = subAssetsToSearch[i].subAsset;
                        if (asset == null || asset.Equals(null)) continue;

                        assetsSet.Add(asset);
                        allAssetClasses.Add(asset.GetType());

                        if (asset is MonoScript)
                        {
                            Type monoScriptType = ((MonoScript) asset).GetClass();
                            if (monoScriptType != null && typeof(Component).IsAssignableFrom(monoScriptType))
                                allAssetClasses.Add(monoScriptType);
                        }
                        else if (asset is GameObject)
                        {
                            Component[] components = ((GameObject) asset).GetComponents<Component>();
                            for (int j = 0; j < components.Length; j++)
                            {
                                if (components[j] == null || components[j].Equals(null)) continue;
                                assetsSet.Add(components[j]);
                                allAssetClasses.Add(components[j].GetType());
                            }
                        }
                    }
                }
            }
            finally
            {
                subAssetsToSearch.RemoveRange(subAssetsToSearch.Count - assetsToSearch.Count, assetsToSearch.Count);
            }

            assetClasses = new Type[allAssetClasses.Count];
            allAssetClasses.CopyTo(assetClasses);

            foreach (Object asset in assetsSet)
            {
                searchedObjects.Add(asset.Hash(), new Node(asset));

                if (asset is Texture)
                {
                    searchMaterialAssets = true;
                    searchRenderers = true;
                    searchMaterialsForTexture = true;
                }
                else if (asset is Material)
                {
                    searchRenderers = true;
                }
                else if (asset is MonoScript)
                {
                    searchMonoBehavioursForScript = true;
                }
                else if (asset is Shader)
                {
                    searchMaterialAssets = true;
                    searchRenderers = true;
                    searchMaterialsForShader = true;
                }
                else if (asset is GameObject)
                {
                    searchGameObjectReferences = true;
                }
            }

            HashSet<string> scenesToSearch = new HashSet<string>();

            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                Scene scene = EditorSceneManager.GetSceneAt(i);
                if (scene.IsValid()) scenesToSearch.Add(scene.path);
            }

            searchSerializableVariablesOnly = !EditorApplication.isPlaying;

            foreach (string scenePath in scenesToSearch)
            {
                if (scenePath != null) SearchScene(scenePath);
            }

            if (EditorApplication.isPlaying)
            {
                currentReferenceHolder = new ReferenceHolder("DontDestroyOnLoad", false);

                GameObject[] rootGameObjects = GetDontDestroyOnLoadObjects().ToArray();
                for (int i = 0; i < rootGameObjects.Length; i++) SearchGameObjectRecursively(rootGameObjects[i]);

                if (currentReferenceHolder.NumberOfReferences > 0) searchResult.Add(currentReferenceHolder);
            }

            for (int i = 0; i < searchResult.Count; i++) searchResult[i].InitializeNodes();

            currentPhase = Phase.Complete;
        }

        private void SearchScene(string scenePath)
        {
            Scene scene = EditorSceneManager.GetSceneByPath(scenePath);

            if (!EditorApplication.isPlaying)
            {
                if (searchedType == SearchedType.Asset &&
                    !AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath).HasAnyReferenceTo(assetsSet))
                    return;

                scene = EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
            }

            currentReferenceHolder = new ReferenceHolder(scenePath, true);

            GameObject[] rootGameObjects = scene.GetRootGameObjects();
            for (int i = 0; i < rootGameObjects.Length; i++) SearchGameObjectRecursively(rootGameObjects[i]);

            if (currentReferenceHolder.NumberOfReferences == 0)
            {
                if (!EditorApplication.isPlaying)
                {
                    bool sceneIsOneOfInitials = false;
                    for (int i = 0; i < initialSceneSetup.Length; i++)
                    {
                        if (initialSceneSetup[i].path == scenePath)
                        {
                            if (!initialSceneSetup[i].isLoaded) EditorSceneManager.CloseScene(scene, false);
                            sceneIsOneOfInitials = true;
                            break;
                        }
                    }

                    if (!sceneIsOneOfInitials) EditorSceneManager.CloseScene(scene, true);
                }
            }
            else
            {
                searchResult.Add(currentReferenceHolder);
            }
        }

        private void SearchGameObjectRecursively(GameObject go)
        {
            BeginSearchObject(go);

            Transform tr = go.transform;
            for (int i = 0; i < tr.childCount; i++) SearchGameObjectRecursively(tr.GetChild(i).gameObject);
        }

        private void BeginSearchObject(Object obj)
        {
            if (assetsSet.Contains(obj))
            {
                if (obj is GameObject && !obj.IsAsset())
                {
                    Node node = PopReferenceNode(obj);
                    Component[] components = ((GameObject) obj).GetComponents<Component>();
                    for (int i = 0; i < components.Length; i++)
                    {
                        Node componentNode = SearchComponent(components[i]);
                        if (componentNode == null) continue;

                        if (componentNode.NumberOfOutgoingLinks > 0) node.AddLinkTo(componentNode);
                        else PoolReferenceNode(componentNode);
                    }

                    if (node.NumberOfOutgoingLinks > 0) currentReferenceHolder.AddReference(node);
                    else PoolReferenceNode(node);
                }

                return;
            }

            Node searchResult = SearchObject(obj);
            if (searchResult != null) currentReferenceHolder.AddReference(searchResult);
        }

        private Node SearchObject(object obj)
        {
            if (obj == null || obj.Equals(null)) return null;
            if (callStack.Contains(obj)) return null;

            string objHash = null;
            if (!(obj is ValueType))
            {
                objHash = obj.Hash();
                Node cachedResult;
                if (searchedObjects.TryGetValue(objHash, out cachedResult))
                {
                    return cachedResult ?? null;
                }
            }

            Node result;
            Object unityObject = obj as Object;
            if (unityObject != null)
            {
                if (assetsSet.Contains(unityObject))
                {
                    result = new Node(unityObject);
                    searchedObjects.Add(objHash, result);
                    return result;
                }

                if (searchedType == SearchedType.Asset && !unityObject.HasAnyReferenceTo(assetsSet))
                {
                    searchedObjects.Add(objHash, null);
                    return null;
                }

                callStack.Push(unityObject);

                if (unityObject is GameObject) result = SearchGameObject((GameObject) unityObject);
                else if (unityObject is Component) result = SearchComponent((Component) unityObject);
                else if (unityObject is Material) result = SearchMaterial((Material) unityObject);
                else if (unityObject is RuntimeAnimatorController)
                    result = SearchAnimatorController((RuntimeAnimatorController) unityObject);
                else if (unityObject is AnimationClip) result = SearchAnimationClip((AnimationClip) unityObject);
                else
                {
                    result = PopReferenceNode(unityObject);
                    SearchFieldsAndPropertiesOf(result);
                }

                callStack.Pop();
            }
            else
            {
                if (currentDepth >= searchDepthLimit) return null;

                callStack.Push(obj);
                currentDepth++;

                result = PopReferenceNode(obj);
                SearchFieldsAndPropertiesOf(result);

                currentDepth--;
                callStack.Pop();
            }

            if (result != null && result.NumberOfOutgoingLinks == 0)
            {
                PoolReferenceNode(result);
                result = null;
            }

            if ((result != null || unityObject != null || currentDepth == 0) && objHash != null)
                searchedObjects.Add(objHash, result);

            return result;
        }

        private Node SearchGameObject(GameObject go)
        {
            Node node = PopReferenceNode(go);

            if (searchGameObjectReferences)
            {
                Object prefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
                if (assetsSet.Contains(prefab) && go == PrefabUtility.FindRootGameObjectWithSameParentPrefab(go))
                    node.AddLinkTo(GetReferenceNode(prefab), "Prefab object");
            }

            Component[] components = go.GetComponents<Component>();
            for (int i = 0; i < components.Length; i++)
            {
                Node result = SearchObject(components[i]);
                if (result != null) node.AddLinkTo(result);
            }

            return node;
        }

        private Node SearchComponent(Component component)
        {
            if (component is Transform)
                return null;

            Node node = PopReferenceNode(component);

            if (searchMonoBehavioursForScript && component is MonoBehaviour)
            {
                MonoScript script = MonoScript.FromMonoBehaviour((MonoBehaviour) component);
                if (assetsSet.Contains(script))
                    node.AddLinkTo(GetReferenceNode(script));
            }

            if (searchRenderers && component is Renderer)
            {
                Material[] materials = ((Renderer) component).sharedMaterials;
                for (int j = 0; j < materials.Length; j++)
                    node.AddLinkTo(SearchObject(materials[j]));
            }

            if (component is Animation)
            {
                foreach (AnimationState anim in (Animation) component)
                    node.AddLinkTo(SearchObject(anim.clip));
            }

            if (component is Animator)
            {
                node.AddLinkTo(SearchObject(((Animator) component).runtimeAnimatorController));
            }

            SearchFieldsAndPropertiesOf(node);

            return node;
        }

        private Node SearchMaterial(Material material)
        {
            Node node = PopReferenceNode(material);

            if (searchMaterialsForShader && assetsSet.Contains(material.shader))
                node.AddLinkTo(GetReferenceNode(material.shader), "Shader");

            if (searchMaterialsForTexture)
            {
                Shader shader = material.shader;
                int shaderPropertyCount = ShaderUtil.GetPropertyCount(shader);
                for (int k = 0; k < shaderPropertyCount; k++)
                {
                    if (ShaderUtil.GetPropertyType(shader, k) == ShaderUtil.ShaderPropertyType.TexEnv)
                    {
                        string propertyName = ShaderUtil.GetPropertyName(shader, k);
                        Texture assignedTexture = material.GetTexture(propertyName);
                        if (assetsSet.Contains(assignedTexture))
                            node.AddLinkTo(GetReferenceNode(assignedTexture),
                                "Shader property: " + propertyName);
                    }
                }
            }

            return node;
        }

        private Node SearchAnimatorController(RuntimeAnimatorController controller)
        {
            Node node = PopReferenceNode(controller);

            AnimationClip[] animClips = controller.animationClips;
            for (int j = 0; j < animClips.Length; j++)
                node.AddLinkTo(SearchObject(animClips[j]));

            return node;
        }

        private Node SearchAnimationClip(AnimationClip clip)
        {
            Node node = PopReferenceNode(clip);

            EditorCurveBinding[] objectCurves = AnimationUtility.GetObjectReferenceCurveBindings(clip);
            for (int i = 0; i < objectCurves.Length; i++)
            {
                ObjectReferenceKeyframe[] keyframes = AnimationUtility.GetObjectReferenceCurve(clip, objectCurves[i]);
                for (int j = 0; j < keyframes.Length; j++)
                    node.AddLinkTo(SearchObject(keyframes[j].value), "Keyframe: " + keyframes[j].time);
            }

            return node;
        }

        private void SearchFieldsAndPropertiesOf(Node node)
        {
            VariableGetterHolder[] variables = GetFilteredVariablesForType(node.nodeObject.GetType());
            for (int i = 0; i < variables.Length; i++)
            {
                if (searchSerializableVariablesOnly && !variables[i].isSerializable)
                    continue;

                try
                {
                    object variableValue = variables[i].Get(node.nodeObject);
                    if (variableValue == null)
                        continue;

                    if (!(variableValue is IEnumerable) || variableValue is Transform)
                        node.AddLinkTo(SearchObject(variableValue),
                            (variables[i].isProperty ? "Property: " : "Variable: ") + variables[i].name);
                    else
                    {
                        foreach (object arrayItem in (IEnumerable) variableValue)
                            node.AddLinkTo(SearchObject(arrayItem),
                                (variables[i].isProperty ? "Property (IEnumerable): " : "Variable (IEnumerable): ") +
                                variables[i].name);
                    }
                }
                catch (UnassignedReferenceException)
                {
                }
                catch (MissingReferenceException)
                {
                }
            }
        }

        private VariableGetterHolder[] GetFilteredVariablesForType(Type type)
        {
            VariableGetterHolder[] result;
            if (typeToVariables.TryGetValue(type, out result))
                return result;
            validVariables.Clear();

            if (fieldModifiers != (BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                Type currType = type;
                while (currType != typeof(object))
                {
                    FieldInfo[] fields = currType.GetFields(fieldModifiers);
                    for (int i = 0; i < fields.Length; i++)
                    {
                        if (Attribute.IsDefined(fields[i], typeof(ObsoleteAttribute)))
                            continue;
                        Type fieldType = fields[i].FieldType;
                        if (fieldType.IsPrimitive || fieldType == typeof(string) || fieldType.IsEnum)
                            continue;

#if USE_EXPERIMENTAL_METHOD
						if( !IsTypeSearchable( fieldType ) )
							continue;
#else
                        if (fieldType.IsPrimitiveUnityType())
                            continue;
#endif

                        VariableGetVal getter = fields[i].GetValue;

                        if (getter != null)
                            validVariables.Add(new VariableGetterHolder(fields[i], getter, fields[i].IsSerializable()));
                    }

                    currType = currType.BaseType;
                }
            }


            if (propertyModifiers != (BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                Type currType = type;
                while (currType != typeof(object))
                {
                    PropertyInfo[] properties = currType.GetProperties(propertyModifiers);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (Attribute.IsDefined(properties[i], typeof(ObsoleteAttribute))) continue;

                        Type propertyType = properties[i].PropertyType;
                        if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType.IsEnum) continue;

#if USE_EXPERIMENTAL_METHOD
						if( !IsTypeSearchable( propertyType ) ) continue;
#else
                        if (propertyType.IsPrimitiveUnityType()) continue;
#endif

                        Type declaringType = properties[i].DeclaringType;
                        bool nameEquals = PropertyEquals(properties[i], new[]
                        {
                            "gameObject", "transform", "attachedRigidbody", "rectTransform", "canvasRenderer", "canvas",
                            "mesh", "sharedMaterial", "sharedMaterials", "material", "materials"
                        });


                        if (typeof(Component).IsAssignableFrom(currType)) continue;
                        if (typeof(UnityEngine.UI.Graphic).IsAssignableFrom(currType)) continue;
                        if (typeof(MeshFilter).IsAssignableFrom(currType)) continue;
                        if (typeof(Renderer).IsAssignableFrom(currType)) continue;
                        if (typeof(Renderer).IsAssignableFrom(currType)) continue;
                        if (typeof(Collider).IsAssignableFrom(currType)) continue;
                        if (typeof(Collider2D).IsAssignableFrom(currType)) continue;
                        if (declaringType == typeof(Navigation)) continue;
                        if (declaringType == typeof(SpriteState)) continue;
                        if (!nameEquals)
                        {
                            validVariables.Add(new VariableGetterHolder(properties[i], properties[i].CreateGetter(),
                                properties[i].IsSerializable()));
                        }
                    }

                    currType = currType.BaseType;
                }
            }

            result = validVariables.ToArray();
            typeToVariables.Add(type, result);

            return result;
        }

        private bool PropertyEquals(PropertyInfo property, string[] propertyEqualsNames)
        {
            string propertyName = property.Name;
            for (int i = 0; i < propertyEqualsNames.Length; i++)
            {
                if (propertyName.Equals(propertyEqualsNames[i])) return true;
            }

            return false;
        }

        private Node GetReferenceNode(object nodeObject, string hash = null)
        {
            if (hash == null) hash = nodeObject.Hash();

            Node result;

            if (!searchedObjects.TryGetValue(hash, out result) || result == null)
            {
                result = PopReferenceNode(nodeObject);
                searchedObjects[hash] = result;
            }

            return result;
        }

        private Node PopReferenceNode(object nodeObject)
        {
            if (nodesPool.Count == 0)
            {
                for (int i = 0; i < 32; i++) nodesPool.Add(new Node());
            }

            int index = nodesPool.Count - 1;
            Node node = nodesPool[index];
            node.nodeObject = nodeObject;
            nodesPool.RemoveAt(index);

            return node;
        }

        private void PoolReferenceNode(Node node)
        {
            node.Clear();
            nodesPool.Add(node);
        }

        private List<GameObject> GetDontDestroyOnLoadObjects()
        {
            List<GameObject> rootGameObjectsExceptDontDestroyOnLoad = new List<GameObject>();
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                rootGameObjectsExceptDontDestroyOnLoad.AddRange(SceneManager.GetSceneAt(i).GetRootGameObjects());
            }

            List<GameObject> rootGameObjects = new List<GameObject>();
            Transform[] allTransforms = Resources.FindObjectsOfTypeAll<Transform>();
            for (int i = 0; i < allTransforms.Length; i++)
            {
                Transform root = allTransforms[i].root;
                if (root.hideFlags == HideFlags.None && !rootGameObjects.Contains(root.gameObject))
                {
                    rootGameObjects.Add(root.gameObject);
                }
            }

            return rootGameObjects.Where(t => !rootGameObjectsExceptDontDestroyOnLoad.Contains(t)).ToList();
        }
    }

    [Serializable]
    public class SubAssetToSearch
    {
        public Object subAsset;
        public bool shouldSearch;

        public SubAssetToSearch(Object subAsset, bool shouldSearch)
        {
            this.subAsset = subAsset;
            this.shouldSearch = shouldSearch;
        }
    }
}