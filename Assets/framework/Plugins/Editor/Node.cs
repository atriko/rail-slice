﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Plugins.Editor
{
    public class Node
    {
        public struct Link
        {
            public readonly Node targetNode;
            public readonly string description;

            public Link(Node targetNode, string description)
            {
                this.targetNode = targetNode;
                this.description = description;
            }
        }

        private static int uid_last = 0;
        private readonly int uid;

        public object nodeObject;
        private readonly List<Link> links;

        private int? instanceId;
        private string description;

        public Object SelectedEditorObject
        {
            get { return instanceId.HasValue ? EditorUtility.InstanceIDToObject(instanceId.Value) : null; }
        }

        public int NumberOfOutgoingLinks
        {
            get { return links.Count; }
        }

        public Link this[int index]
        {
            get { return links[index]; }
        }

        public Node()
        {
            links = new List<Link>(2);
            uid = uid_last++;
        }

        public Node(object obj) : this()
        {
            nodeObject = obj;
        }

        public void AddLinkTo(Node nextNode, string description = null)
        {
            if (nextNode != null)
            {
                if (!string.IsNullOrEmpty(description))
                    description = "[" + description + "]";

                links.Add(new Link(nextNode, description));
            }
        }

        public void InitializeRecursively()
        {
            if (description != null)
                return;

            Object unityObject = nodeObject as Object;
            if (unityObject != null)
            {
                instanceId = unityObject.GetInstanceID();
                description = unityObject.name + " (" + unityObject.GetType() + ")";
            }
            else if (nodeObject != null)
            {
                instanceId = null;
                description = nodeObject.GetType() + " object";
            }
            else
            {
                instanceId = null;
                description = "<<destroyed>>";
            }

            nodeObject = null;

            for (int i = 0; i < links.Count; i++)
                links[i].targetNode.InitializeRecursively();
        }

        public void Clear()
        {
            nodeObject = null;
            links.Clear();
        }

        public void DrawOnGUIRecursively(string linkToPrevNodeDescription = null)
        {
            GUILayout.BeginHorizontal();

            DrawOnGUI(linkToPrevNodeDescription);

            if (links.Count > 0)
            {
                GUILayout.BeginVertical();

                for (int i = 0; i < links.Count; i++)
                {
                    Node next = links[i].targetNode;
                    next.DrawOnGUIRecursively(links[i].description);
                }

                GUILayout.EndVertical();
            }

            GUILayout.EndHorizontal();
        }

        public void DrawOnGUI(string linkToPrevNodeDescription)
        {
            string label = GetNodeContent(linkToPrevNodeDescription);
            if (GUILayout.Button(label, AssetUsagePanel.BoxGUIStyle, AssetUsagePanel.GL_EXPAND_HEIGHT))
            {
                SelectedEditorObject.SelectInEditor();
            }
        }

        private string GetNodeContent(string linkToPrevNodeDescription = null)
        {
            if (!string.IsNullOrEmpty(linkToPrevNodeDescription))
                return linkToPrevNodeDescription + "\n" + description;

            return description;
        }

        public int SerializeRecursively(Dictionary<Node, int> nodeToIndex,
            List<ReferenceHolder.SerializableNode> serializedNodes)
        {
            int index;
            if (nodeToIndex.TryGetValue(this, out index))
                return index;

            ReferenceHolder.SerializableNode serializedNode = new ReferenceHolder.SerializableNode()
            {
                instanceId = instanceId ?? 0,
                isUnityObject = instanceId.HasValue,
                description = description
            };

            index = serializedNodes.Count;
            nodeToIndex[this] = index;
            serializedNodes.Add(serializedNode);

            if (links.Count > 0)
            {
                serializedNode.links = new List<int>(links.Count);
                serializedNode.linkDescriptions = new List<string>(links.Count);

                for (int i = 0; i < links.Count; i++)
                {
                    serializedNode.links.Add(links[i].targetNode.SerializeRecursively(nodeToIndex, serializedNodes));
                    serializedNode.linkDescriptions.Add(links[i].description);
                }
            }

            return index;
        }

        public void Deserialize(ReferenceHolder.SerializableNode serializedNode, List<Node> allNodes)
        {
            if (serializedNode.isUnityObject) instanceId = serializedNode.instanceId;
            else instanceId = null;

            description = serializedNode.description;

            if (serializedNode.links != null)
            {
                for (int i = 0; i < serializedNode.links.Count; i++)
                    links.Add(new Link(allNodes[serializedNode.links[i]], serializedNode.linkDescriptions[i]));
            }
        }

        public override int GetHashCode()
        {
            return uid;
        }
    }
}
