﻿using System;
using System.Collections.Generic;
using Plugins.Editor;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace DHFramework.Plugins.Editor
{
    public class AssetFinder
    {
        public enum AssetType
        {
            Texture,
            Material,
            Prefab,
            Script,
            Asset
        }

        public Action<Object> StartSearchEvent;

        private string findName = "";
        private AnimBool hasFilterTypes;
        private Vector2 foundScroll = Vector2.zero;
        private List<FilterToggle> filterToggles;
        private string prebFindName = "";
        private List<PathLabelButton> pathLabels = new List<PathLabelButton>();
        private Object referObj;
        private Object folder;
        private Object selectedObject;
        private EditorWindow window;

        void AssetFinderInit()
        {
            filterToggles = new List<FilterToggle>();
            filterToggles.Add(new FilterToggle(true, "Textrue", AssetType.Texture));
            filterToggles.Add(new FilterToggle(true, "Material", AssetType.Material));
            filterToggles.Add(new FilterToggle(true, "Prefab", AssetType.Prefab));
            filterToggles.Add(new FilterToggle(true, "Script", AssetType.Script));
            filterToggles.Add(new FilterToggle(true, "Asset", AssetType.Asset));

            hasFilterTypes = new AnimBool(false);
            hasFilterTypes.valueChanged.AddListener(window.Repaint);

            referObj = null;
        }

        public AssetFinder(EditorWindow window)
        {
            this.window = window;

            AssetFinderInit();
            FilterToggle.isChanged = true;
            prebFindName = string.Empty;
        }

        protected class FilterToggle
        {
            public static bool isChanged = false;
            public AssetType type;
            public bool st;
            public string text;

            public FilterToggle(bool _st, string _text, AssetType _type)
            {
                st = _st;
                text = _text;
                type = _type;
            }
        }

        public void ShowInputMode()
        {
            GUILayout.BeginHorizontal();
            findName = EditorGUILayout.TextField("", findName, AssetUsagePanel.BoxGUIStyle,
                AssetUsagePanel.GL_HEIGHT_40, GUILayout.Width(window.position.width * 0.8f));

            if (findName == "") selectedObject = null;

            GUI.color = Color.green;

            if (GUILayout.Button("START SEARCH", AssetUsagePanel.BoxGUIStyle, AssetUsagePanel.GL_EXPAND_WIDTH,
                AssetUsagePanel.GL_HEIGHT_40))
            {
                GUI.color = Color.red;

                if (selectedObject == null)
                {
                    window.ShowNotification(new GUIContent("ADD AN ASSET TO THE LIST FIRST!"));
                }
                else if (!EditorApplication.isPlaying && !AreScenesSaved())
                {
                    window.ShowNotification(new GUIContent("SAVE OPEN SCENES FIRST!"));
                }
                else
                {
                    StartSearchEvent?.Invoke(selectedObject);
                }
            }

            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            foundScroll = EditorGUILayout.BeginScrollView(foundScroll, false, true,
                GUILayout.Width(window.position.width),
                GUILayout.Height(window.position.height - 50));

            if (prebFindName.Equals(findName) && !FilterToggle.isChanged)
            {
                foreach (var label in pathLabels)
                {
                    label.ShowButton();
                }
            }
            else
            {
                CreatePathLabel(findName);
            }


            EditorGUILayout.EndScrollView();
            FilterToggle.isChanged = false;
        }


        string GetTypeString(AssetType type)
        {
            switch (type)
            {
                case AssetType.Texture: return "texture2D";
                case AssetType.Material: return "material";
                case AssetType.Prefab: return "prefab";
                case AssetType.Script: return "script";
                case AssetType.Asset: return "asset";
            }

            return "";
        }

        void CreatePathLabel(string name)
        {
            pathLabels.Clear();

            if (name.Length < 1)
            {
                return;
            }

            if (hasFilterTypes.value)
            {
                foreach (var filter in filterToggles)
                {
                    if (filter.st)
                    {
                        EditorGUILayout.TextField("===" + filter.text + "===\n", EditorStyles.boldLabel);
                        FindAndCreateLabel(name, GetTypeString(filter.type));
                    }
                }
            }
            else
            {
                FindAndCreateLabel(name);
            }

            prebFindName = name;
        }

        protected class PathLabelButton
        {
            public string path;
            public string guid;
            public string type;
            public GUIStyle style;
            public System.Action<PathLabelButton> callback;

            private bool selectedPath;

            public PathLabelButton(string _path, string _guid, string _type)
            {
                path = _path;
                guid = _guid;
                type = _type;
                style = new GUIStyle();
                style.normal.textColor = Color.white;
            }

            public void SetActive(bool value)
            {
                selectedPath = value;
            }

            public void ShowButton()
            {
                GUI.color = selectedPath ? Color.cyan : Color.white;
                if (GUILayout.Button(path, AssetUsagePanel.BoxGUIStyle, AssetUsagePanel.GL_EXPAND_WIDTH,
                    AssetUsagePanel.GL_HEIGHT_40)) callback(this);
            }
        }

        void FindAndCreateLabel(string name, string type = "")
        {
            string filter = name;
            if (type != "") filter += " t:" + type;

            string[] guids = AssetDatabase.FindAssets(filter);

            if (guids != null && guids.Length > 0)
            {
                foreach (var guid in guids)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guid);
                    var pathLabel = new PathLabelButton(path, guid, type);
                    pathLabel.callback = CheckUsing;
                    pathLabels.Add(pathLabel);
                    pathLabel.ShowButton();
                }
            }
        }

        void CheckUsing(PathLabelButton pathLabel)
        {
            foreach (var label in pathLabels)
            {
                if (!pathLabel.Equals(label)) label.SetActive(false);
            }

            pathLabel.SetActive(true);
            selectedObject = AssetDatabase.LoadAssetAtPath(pathLabel.path, typeof(Object));
        }

        private bool AreScenesSaved()
        {
            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                Scene scene = EditorSceneManager.GetSceneAt(i);
                if (scene.isDirty || string.IsNullOrEmpty(scene.path)) return false;
            }

            return true;
        }
    }
}