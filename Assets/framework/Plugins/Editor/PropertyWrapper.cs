﻿using System;
using System.Reflection;

namespace Plugins.Editor
{
    public class PropertyWrapper<TObject, TValue> : IPropertyAccessor where TObject : class
    {
        private readonly Func<TObject, TValue> getter;

        public PropertyWrapper(MethodInfo getterMethod)
        {
            getter = (Func<TObject, TValue>) Delegate.CreateDelegate(typeof(Func<TObject, TValue>), getterMethod);
        }

        public object GetValue(object obj)
        {
            try
            {
                return getter((TObject) obj);
            }
            catch
            {
                return null;
            }
        }
    }
}