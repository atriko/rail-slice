﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Plugins.Editor
{
    public static class AssetUsageDetector
    {
        private static HashSet<Type> types = new HashSet<Type>()
        {
            typeof(string), typeof(Vector3), typeof(Vector2), typeof(Rect),
            typeof(Quaternion), typeof(Color), typeof(Color32), typeof(LayerMask), typeof(Vector4),
            typeof(Matrix4x4), typeof(AnimationCurve), typeof(Gradient), typeof(RectOffset)
        };

        public static string Hash(this object obj)
        {
            if (obj is Object) return obj.GetHashCode().ToString();
            return obj.GetHashCode() + obj.GetType().Name;
        }

        public static bool IsAsset(this object obj)
        {
            return obj is Object && !string.IsNullOrEmpty(AssetDatabase.GetAssetPath((Object) obj));
        }

        public static void SelectInEditor(this Object obj)
        {
            if (obj == null) return;

            Event e = Event.current;

            if (!e.control)
            {
                obj.PingInEditor();
                Selection.activeObject = obj;
            }
            else
            {
                Component objAsComp = obj as Component;
                GameObject objAsGO = obj as GameObject;
                int selectionIndex = -1;

                Object[] selection = Selection.objects;
                for (int i = 0; i < selection.Length; i++)
                {
                    Object selected = selection[i];

                    if (selected == obj || (objAsComp != null && selected == objAsComp.gameObject) ||
                        (objAsGO != null && selected is Component && ((Component) selected).gameObject == objAsGO))
                    {
                        selectionIndex = i;
                        break;
                    }
                }

                Object[] newSelection;
                if (selectionIndex == -1)
                {
                    newSelection = new Object[selection.Length + 1];
                    selection.CopyTo(newSelection, 0);
                    newSelection[selection.Length] = obj;
                }
                else
                {
                    newSelection = new Object[selection.Length - 1];
                    int j = 0;
                    for (int i = 0; i < selectionIndex; i++, j++) newSelection[j] = selection[i];
                    for (int i = selectionIndex + 1; i < selection.Length; i++, j++) newSelection[j] = selection[i];
                }

                Selection.objects = newSelection;
            }
        }


        public static void PingInEditor(this Object obj)
        {
            if (obj is Component) obj = ((Component) obj).gameObject;
            if (obj.IsAsset() && obj is GameObject)
            {
                Transform objTransform = ((GameObject) obj).transform;
                while (objTransform.parent != null && objTransform.parent.parent != null)
                {
                    objTransform = objTransform.parent;
                }

                obj = objTransform.gameObject;
            }

            EditorGUIUtility.PingObject(obj);
        }

        public static bool HasAnyReferenceTo(this Object obj, HashSet<Object> references)
        {
            Object[] dependencies = EditorUtility.CollectDependencies(new[] {obj});
            for (int i = 0; i < dependencies.Length; i++)
            {
                if (references.Contains(dependencies[i])) return true;
            }

            return false;
        }

        public static bool IsSerializable(this FieldInfo fieldInfo)
        {
            if (fieldInfo.IsInitOnly || ((!fieldInfo.IsPublic || fieldInfo.IsNotSerialized) &&
                                         !Attribute.IsDefined(fieldInfo, typeof(SerializeField))))
                return false;

            return IsTypeSerializable(fieldInfo.FieldType);
        }

        public static bool IsSerializable(this PropertyInfo propertyInfo)
        {
            return IsTypeSerializable(propertyInfo.PropertyType);
        }

        private static bool IsTypeSerializable(Type type)
        {
            if (typeof(Object).IsAssignableFrom(type)) return true;

            if (type.IsArray)
            {
                if (type.GetArrayRank() != 1) return false;
                type = type.GetElementType();
                if (typeof(Object).IsAssignableFrom(type)) return true;
            }
            else if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition() != typeof(List<>)) return false;
                type = type.GetGenericArguments()[0];
                if (typeof(Object).IsAssignableFrom(type)) return true;
            }

            if (type.IsGenericType) return false;
            return Attribute.IsDefined(type, typeof(SerializableAttribute), false);
        }

        public static bool IsPrimitiveUnityType(this Type type)
        {
            return type.IsPrimitive || types.Contains(type);
        }

        public static VariableGetVal CreateGetter(this PropertyInfo propertyInfo)
        {
            if (propertyInfo.GetIndexParameters().Length > 0) return null;

            MethodInfo methodInfo = propertyInfo.GetGetMethod(true);

            if (methodInfo != null)
            {
                Type genericType =
                    typeof(PropertyWrapper<,>).MakeGenericType(propertyInfo.DeclaringType,
                        propertyInfo.PropertyType);
                return ((IPropertyAccessor) Activator.CreateInstance(genericType, methodInfo)).GetValue;
            }

            return null;
        }
    }
}