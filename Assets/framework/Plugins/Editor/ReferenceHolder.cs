﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Plugins.Editor
{
    [Serializable]
    public class ReferenceHolder : ISerializationCallbackReceiver
    {
        public struct ReferencePath
        {
            public readonly Node StartNode;
            public readonly int[] PathLinksToFollow;

            public ReferencePath(Node startNode, int[] pathIndices)
            {
                this.StartNode = startNode;
                PathLinksToFollow = pathIndices;
            }
        }

        [Serializable]
        public class SerializableNode
        {
            public int instanceId;
            public bool isUnityObject;
            public string description;

            public List<int> links;
            public List<string> linkDescriptions;
        }

        private string title;
        private bool clickable;
        private List<Node> references;
        public List<ReferencePath> referencePathsShortUnique;
        public List<ReferencePath> referencePathsShortest;

        private List<SerializableNode> serializedNodes;
        private List<int> initialSerializedNodes;

        public int NumberOfReferences
        {
            get { return references.Count; }
        }

        public ReferenceHolder(string title, bool clickable)
        {
            this.title = title;
            this.clickable = clickable;
            references = new List<Node>();
            referencePathsShortUnique = null;
            referencePathsShortest = null;
        }

        public void AddReference(Node node)
        {
            references.Add(node);
        }

        public void InitializeNodes()
        {
            for (int i = 0; i < references.Count; i++) references[i].InitializeRecursively();
        }

        public void DrawOnGUI()
        {
            Color c = GUI.color;
            GUI.color = Color.cyan;

            if (GUILayout.Button(title, AssetUsagePanel.BoxGUIStyle, AssetUsagePanel.GL_EXPAND_WIDTH,
                    AssetUsagePanel.GL_HEIGHT_40) && clickable)
            {
                AssetDatabase.LoadAssetAtPath<SceneAsset>(title).SelectInEditor();
            }

            GUI.color = Color.yellow;

            for (int i = 0; i < references.Count; i++)
            {
                GUILayout.Space(10);

                references[i].DrawOnGUIRecursively();
            }

            GUI.color = c;
        }

        public void OnBeforeSerialize()
        {
            if (references == null)
                return;

            if (serializedNodes == null)
                serializedNodes = new List<SerializableNode>(references.Count * 5);
            else
                serializedNodes.Clear();

            if (initialSerializedNodes == null)
                initialSerializedNodes = new List<int>(references.Count);
            else
                initialSerializedNodes.Clear();

            Dictionary<Node, int> nodeToIndex = new Dictionary<Node, int>(references.Count * 5);
            for (int i = 0; i < references.Count; i++)
                initialSerializedNodes.Add(references[i].SerializeRecursively(nodeToIndex, serializedNodes));
        }

        public void OnAfterDeserialize()
        {
            if (initialSerializedNodes == null || serializedNodes == null)
                return;

            if (references == null)
                references = new List<Node>(initialSerializedNodes.Count);
            else
                references.Clear();

            List<Node> allNodes = new List<Node>(serializedNodes.Count);
            for (int i = 0; i < serializedNodes.Count; i++)
                allNodes.Add(new Node());

            for (int i = 0; i < serializedNodes.Count; i++)
                allNodes[i].Deserialize(serializedNodes[i], allNodes);

            for (int i = 0; i < initialSerializedNodes.Count; i++)
                references.Add(allNodes[initialSerializedNodes[i]]);

            referencePathsShortUnique = null;
            referencePathsShortest = null;

            serializedNodes.Clear();
            initialSerializedNodes.Clear();
        }
    }
}