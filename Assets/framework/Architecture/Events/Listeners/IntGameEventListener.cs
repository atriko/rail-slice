﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "int Event Listener")]
    public sealed class IntGameEventListener : BaseGameEventListener<int, IntGameEvent, IntUnityEvent>
    {
        [SerializeField] private IntUnityEvent response;

        protected override void OnResponse(int value)
        {
            response.Invoke(value);
        }
    }
}