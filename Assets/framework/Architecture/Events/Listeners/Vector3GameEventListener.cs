using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Vector3 Event Listener")]
    public sealed class Vector3GameEventListener : BaseGameEventListener<Vector3, Vector3GameEvent, Vector3UnityEvent>
    {
        [SerializeField] private Vector3UnityEvent response;

        protected override void OnResponse(Vector3 value)
        {
            response.Invoke(value);
        }
    }
}