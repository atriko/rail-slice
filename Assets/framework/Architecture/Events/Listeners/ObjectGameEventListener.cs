﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Object Event Listener")]
    public class ObjectGameEventListener : BaseGameEventListener<Object, ObjectGameEvent, ObjectUnityEvent>
    {
        [SerializeField] private ObjectUnityEvent response;

        protected override void OnResponse(Object value)
        {
            response.Invoke(value);
        }
    }
}