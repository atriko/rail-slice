using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Transform")]
	public sealed class TransformGameEventListener : BaseGameEventListener<Transform, TransformGameEvent, TransformUnityEvent>
	{
		[SerializeField] private TransformUnityEvent response;

        protected override void OnResponse(Transform value)
        {
            response.Invoke(value);
        }
	}
}