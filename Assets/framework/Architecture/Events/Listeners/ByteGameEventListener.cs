﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "byte Event Listener")]
    public sealed class ByteGameEventListener : BaseGameEventListener<byte, ByteGameEvent, ByteUnityEvent>
    {
        [SerializeField] private ByteUnityEvent response;

        protected override void OnResponse(byte value)
        {
            response.Invoke(value);
        }
    }
}