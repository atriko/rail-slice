using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "Vector2GameEvent.asset",
        menuName = Architecture_Utility.GAME_EVENT + "Structs/Vector2",
        order = Architecture_Utility.ASSET_MENU_ORDER_EVENTS + 10)]
    public sealed class Vector2GameEvent : GameEventBase<Vector2>
    {
    } 
}