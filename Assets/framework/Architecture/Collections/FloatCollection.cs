using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "FloatCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "float",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 3)]
    public class FloatCollection : Collection<float>
    {
    } 
}