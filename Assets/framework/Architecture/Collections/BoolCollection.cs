using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "BoolCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "bool",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 5)]
    public class BoolCollection : Collection<bool>
    {
    } 
}