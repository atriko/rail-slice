using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "IntCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "int",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 4)]
    public class IntCollection : Collection<int>
    {
    } 
}