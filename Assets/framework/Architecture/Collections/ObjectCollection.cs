﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "ObjectCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "Object",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 1)]
    public class ObjectCollection : Collection<Object>
    {
    } 
}