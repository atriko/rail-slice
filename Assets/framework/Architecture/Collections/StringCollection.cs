using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "StringCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "string",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 2)]
    public class StringCollection : Collection<string>
    {
    } 
}