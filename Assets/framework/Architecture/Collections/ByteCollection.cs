using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "ByteCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "byte",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 6)]
    public class ByteCollection : Collection<byte>
    {
    } 
}