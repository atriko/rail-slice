using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "UShortCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "ushort",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 18)]
    public class UShortCollection : Collection<ushort>
    {
    } 
}