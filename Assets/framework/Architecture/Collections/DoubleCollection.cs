using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "DoubleCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "double",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 8)]
    public class DoubleCollection : Collection<double>
    {
    } 
}