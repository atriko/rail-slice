﻿using Plugins.Editor;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;

namespace DHFramework.Editor
{
    public class FrameworkToolbar
    {
        static class ToolbarStyles
        {
            public static readonly GUIStyle commandButtonStyle;

            static ToolbarStyles()
            {
                commandButtonStyle = new GUIStyle(EditorStyles.toolbarButton)
                {
                    fontSize = 16,
                    alignment = TextAnchor.MiddleCenter,
                    imagePosition = ImagePosition.ImageAbove,
                    fontStyle = FontStyle.Bold
                };
            }
        }

        [InitializeOnLoad]
        public class LevelLoaderButtons
        {
            static LevelLoaderButtons()
            {
                ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
            }

            static void OnToolbarGUI()
            {
                // var managerTex = EditorGUIUtility.IconContent(@"d_TerrainInspector.TerrainToolSettings On").image;
                // var configText = EditorGUIUtility.IconContent(@"d_UnityEditor.HierarchyWindow").image;
                // var finderTex = EditorGUIUtility.IconContent(@"d_ViewToolZoom On").image;
                // var firstSceneTex = EditorGUIUtility.IconContent(@"ViewToolOrbit On").image;
                // var restartTex = EditorGUIUtility.IconContent(@"RotateTool On").image;
                // var nextLevelTex = EditorGUIUtility.IconContent(@"d_tab_next").image;
                // var prevLevelTex = EditorGUIUtility.IconContent(@"d_tab_prev").image;
                //
                // // GUILayout.FlexibleSpace();
                //
                // if (ToolButton(managerTex, "Managers Panel")) ManagersEditor.Open();
                // if (ToolButton(configText, "Select GameConfigs")) ManagersEditor.SelectGameConfigs();
                // // if (ToolButton(finderTex, "Asset Finder Panel")) AssetUsagePanel.Show();
                //
                // GUI.color = new Color(0.24f, 1f, 0f);
                //
                // if (ToolButton(firstSceneTex, "Open First Scene")) LevelLoaderEditor.ShowGameScene();
                // if (ToolButton(prevLevelTex, "Load Previous Level")) LevelLoaderEditor.LoadPreviousLevel();
                // if (ToolButton(restartTex, "Reset Levels")) LevelLoaderEditor.RestartCurrentLevel();
                // if (ToolButton(nextLevelTex, "Load Next Level")) LevelLoaderEditor.LoadNextLevel();
            }

            private static bool ToolButton(Texture tex, string toolTip)
            {
                GUIStyle style = ToolbarStyles.commandButtonStyle;
                GUIContent content = new GUIContent(tex, toolTip);
                return GUILayout.Button(content,style);
            }
        }
    }
}