﻿using UnityEditor;
using UnityEngine;

namespace Framework.Editor
{
    [CustomEditor(typeof(Transform))]
    public class TransformInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            Transform t = (Transform) target;
            GUI.color = Color.white;
            EditorGUIUtility.LookLikeControls();
            
            EditorGUI.indentLevel = 0;
            Vector3 position = EditorGUILayout.Vector3Field("Position", t.localPosition);
            Vector3 eulerAngles = EditorGUILayout.Vector3Field("Rotation", t.localEulerAngles);
            Vector3 scale = EditorGUILayout.Vector3Field("Scale", t.localScale);
            
            EditorGUIUtility.LookLikeInspector();
            
            if (GUI.changed)
            {
                Undo.RegisterUndo(t, "Transform Change");
                t.localPosition = FixIfNaN(position);
                t.localEulerAngles = FixIfNaN(eulerAngles);
                t.localScale = FixIfNaN(scale);
            }
        
            GUILayout.BeginHorizontal();
        
            GUI.color = new Color(0.5f, 0.8f, 1f);
            if (GUILayout.Button("Reset Position"))
            {
                Undo.RegisterUndo(t, "Reset Position " + t.name);
                t.transform.position = Vector3.zero;
            }
        
            if (GUILayout.Button("Reset Rotation"))
            {
                Undo.RegisterUndo(t, "Reset Rotation " + t.name);
                t.transform.rotation = Quaternion.identity;
            }
        
            if (GUILayout.Button("Reset Scale"))
            {
                Undo.RegisterUndo(t, "Reset Scale " + t.name);
                t.transform.localScale = Vector3.one;
            }
            
            GUILayout.EndHorizontal();

            // GUILayout.BeginHorizontal();

            GUI.color = new Color(0.74f, 1f, 0.4f);
            if (GUILayout.Button("Copy Component"))
            {
                UnityEditorInternal.ComponentUtility.CopyComponent(t.transform);
            }
            if (GUILayout.Button("Paste Component"))
            {
                UnityEditorInternal.ComponentUtility.PasteComponentValues(t.transform);
            }
  
        }
        
        private Vector3 FixIfNaN(Vector3 v)
        {
            if (float.IsNaN(v.x)) v.x = 0;
            if (float.IsNaN(v.y)) v.y = 0;
            if (float.IsNaN(v.z)) v.z = 0;
            return v;
        }
    }
}
