using DHFramework.Game;
using GameAssets.Scripts;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace DHFramework.Editor
{
    public class ManagersEditor : OdinMenuEditorWindow
    {
        // [MenuItem("AtRiKo/GameConfigs %t")]
        public static void SelectGameConfigs()
        {
            GameConfig gameConfig = Resources.Load("GameConfigs") as GameConfig;
            Selection.activeObject = gameConfig;
        }

        [MenuItem("AtRiKo/Managers %t")]
        public static void Open()
        {
            var window = GetWindow<ManagersEditor>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 500);
        }
        
       
        protected override OdinMenuTree BuildMenuTree()
        {
            Managers managers = Resources.Load("Managers") as Managers;
            OdinMenuTree tree = new OdinMenuTree(true) {Config = {DrawSearchToolbar = true}};

            if (managers != null)
            {
                foreach (ScriptableObject manager in managers.GetManagers())
                {
                    tree.Add(manager.name, manager);
                }
            }

            return tree;
        }
    }
}